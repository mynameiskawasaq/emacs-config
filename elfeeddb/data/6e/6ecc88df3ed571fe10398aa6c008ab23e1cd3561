<ul class="org-ul">
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="http://mp.vv.si/blog/emacs/emacs-symbolism/">Emacs symbolism</a></li>
<li><a href="https://www.youtube.com/watch?v=hrCTolWO_sA">Move The Buffer Contents And Cursor Up Or Down By One Line</a> (0:32)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/65ggm4/execute_macro_as_and_emacsscript_help/">Execute macro as and EmacsScript Help</a> &#8211; see comments for tips on scripting</li>
<li><a href="https://www.reddit.com/r/emacs/comments/64pt41/idea_a_standard_set_of_async_hooks_for_keyboard/">Idea: a standard set of async hooks for keyboard macros</a> &#8211; work around it with executing-kbd-macro</li>
<li><a href="https://www.reddit.com/r/emacs/comments/64pgfr/how_to_ignore_whitespaceonly_killring_entries/">How to ignore whitespace-only kill-ring entries?</a> &#8211; filter-buffer-substring-function</li>
<li><a href="https://www.reddit.com/r/emacs/comments/6558wp/running_elisp_commands_in_vim_buffers/">Running elisp commands in vim buffers?</a></li>
</ul>
</li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=a61fa4af2802a47defe61543eb97276d74ba60bd">Lots of frame and window changes</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=88f43dc30cb8d71830e409973cafbaca13a66a45">Scroll right and left with the mouse wheel, touchpad, or tracker</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=695eacc21ea08b7fa080a232eadae881b5295bef">New variable package-gnupghome-dir</a></li>
</ul>
</li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/64tzsb/changing_the_date_rollover_time_for_orgmode/">Changing the date roll-over time for org-mode</a> &#8211; org-extend-today-until</li>
<li><a href="https://www.reddit.com/r/emacs/comments/64qwq5/emacs_orgbable_mysql_no_longer_working/">Emacs Org-Babel MySQL no longer working</a> &#8211; note about incompatible change (org-babel-get-header)</li>
</ul>
</li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://petton.fr/git/nico/Indium#indium----">Indium – JavaScript REPL w/Object Inspection, stepping debugger ++</a> (<a href="https://www.reddit.com/r/emacs/comments/65lftj/indium_javascript_repl_wobject_inspection/">Reddit</a>) &#8211; formerly known as jade</li>
<li><a href="https://github.com/davidshepherd7/terminal-here">Announcing terminal-here: Cross-platform package to open an external terminal in the directory of the current buffer</a> (<a href="https://www.reddit.com/r/emacs/comments/65id4r/announcing_terminalhere_crossplatform_package_to/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/64x6xw/interact_with_an_oracle_db_from_elisp/">Interact with an Oracle DB from elisp</a></li>
</ul>
</li>
<li>Other:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/64nl7g/sticking_with_emacs_as_a_newbie_ideassuggestions/">Sticking with emacs as a newbie: Ideas/suggestions?</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/64pblw/impressive_emacs_plugins/">Impressive emacs plugins</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/65lf9y/show_manpages_always_in_the_same_window/">Show manpages always in the same window</a> &#8211; major-mode-dedication</li>
<li><a href="https://www.reddit.com/r/emacs/comments/64sdyi/emacsel_episode_7/">Emacs.el Episode 7</a> &#8211; collecting questions for Charles Lowell, frontside.io</li>
<li><a href="https://www.reddit.com/r/emacs/comments/65pjtv/a_short_rant_on_other_applications/">A short rant on other applications</a></li>
<li><a href="http://irreal.org/blog/?p=6141">I Stand Corrected…</a> &#8211; enjoying mu4e</li>
<li><a href="https://github.com/AdrieanKhisbe/spacemacs-anki-deck">Anki Deck to learn Spacemacs Bindings</a></li>
</ul>
</li>
<li>New packages:
<ul class="org-ul">
<li><a href="http://melpa.org/#/add-hooks" target="_blank">add-hooks</a>: Functions for setting multiple hooks</li>
<li><a href="http://melpa.org/#/arch-packer" target="_blank">arch-packer</a>: Arch Linux package management frontend</li>
<li><a href="http://melpa.org/#/bitbucket" target="_blank">bitbucket</a>: Bitbucket API wrapper</li>
<li><a href="http://melpa.org/#/flycheck-joker" target="_blank">flycheck-joker</a>: Add Clojure syntax checker (via Joker) to flycheck</li>
<li><a href="http://melpa.org/#/hierarchy" target="_blank">hierarchy</a>: Library to create and display hierarchy structures</li>
<li><a href="http://melpa.org/#/html2org" target="_blank">html2org</a>: Convert html to org format text</li>
<li><a href="http://melpa.org/#/indium" target="_blank">indium</a>: JavaScript Awesome Development Environment</li>
<li><a href="http://melpa.org/#/password-mode" target="_blank">password-mode</a>: Hide password text using overlays</li>
<li><a href="http://melpa.org/#/playerctl" target="_blank">playerctl</a>: Control your music player (e.g. Spotify) with playerctl</li>
<li><a href="http://melpa.org/#/smart-hungry-delete" target="_blank">smart-hungry-delete</a>: smart hungry deletion of whitespace</li>
<li><a href="http://melpa.org/#/sourcetrail" target="_blank">sourcetrail</a>: Communication with Sourcetrail</li>
<li><a href="http://melpa.org/#/spaceline-all-the-icons" target="_blank">spaceline-all-the-icons</a>: A Spaceline theme using All The Icons</li>
<li><a href="http://melpa.org/#/web-narrow-mode" target="_blank">web-narrow-mode</a>: quick narrow code block in web-mode</li>
<li><a href="http://melpa.org/#/zig-mode" target="_blank">zig-mode</a>: A major mode for the Zig programming language</li>
</ul>
</li>
</ul>
<p>Links from <a href="http://reddit.com/r/emacs/new">reddit.com/r/emacs</a>, <a href="http://reddit.com/r/orgmode">/r/orgmode</a>, <a href="http://reddit.com/r/spacemacs">/r/spacemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="http://planet.emacsen.org">planet.emacsen.org</a>, <a href="https://www.youtube.com/results?search_query=emacs&amp;search_sort=video_date_uploaded">Youtube</a>, the changes to the <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">Emacs NEWS file</a>, and <a href="http://lists.gnu.org/archive/html/emacs-devel/2017-04">emacs-devel</a>.</p>
<p><a href="http://sachachua.com/blog/category/emacs-news">Past Emacs News round-ups</a></p>
