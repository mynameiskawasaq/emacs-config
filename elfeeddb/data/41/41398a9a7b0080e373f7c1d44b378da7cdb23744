<p><a href="http://allthingsd.com/files/2013/12/i-7nGs7q3-M.jpg"><img src="http://allthingsd.com/files/2013/12/i-7nGs7q3-M.jpg" alt="i-7nGs7q3-M" width="600" height="400" class="aligncenter size-full wp-image-382249" /></a></p>
<p>Before we end our reign of terror, <em>oops</em>, tech at <strong>AllThingsD</strong>, I wanted to post a few of my favorite videos from <strong>D: All Things Digital</strong> conferences that we have done since 2003.</p>
<p>While we are proud of all we have created on the news site, I think it is fair to say that the conferences have also been pretty dang fine and moreso taken as a whole. While others may try to trot out the phrase going forward, I think it&#8217;s fair to say we have owned &#8220;all things digital&#8221; for the last 11 years.</p>
<p>We&#8217;ve had a panoply of bigs in tech and media up there over those many conferences, all sitting in our signature red Steelcase chairs, with some memorable moments, including: </p>
<p>More than a half-dozen appearances by the late, great Steve Jobs of Apple, including an joint interview with Microsoft&#8217;s Bill Gates; the famous hoodie incident with Facebook&#8217;s Mark Zuckerberg, who managed to ably recover from the <em>very</em> sticky situation; the testy interview with former Hewlett-Packard CEO Carly Fiorina; the hysterical one with former Sony head Howard Stringer; the sassy one from former Yahoo CEO Carol Bartz; the future-is-here one with former DARPA head Regina Dugan; the silent-off with former Groupon CEO Andrew Mason; the geek-out with Hollywood director James Cameron; the epic Elon Musk chat from last year, he of SpaceX and Tesla.</p>
<p>And many, many more, now numbering in the hundreds, most of which <a href="http://allthingsd.com/video/?catname=conferences">you can find here</a>. </p>
<p>We did not publish the videos for the first five conferences, as we did not have a site to post them too, but here are my top seven from each year we did, all joint appearances with Walt Mossberg, as well as one each from the smaller Dive and other conferences, featuring Peter Kafka, Liz Gannes and Ina Fried.</p>
<p><strong>D5 (2007)</strong></p>
<p>Hands down, the historic &#8212; and decidedly poignant &#8212; joint interview of Gates and Jobs:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-60C4F9FA_9AD5_4D04_8BB6_015AEBB1C052.html"></iframe></p>
<p><strong>D6 (2008)</strong></p>
<p>New Corp&#8217;s Rupert Murdoch in a surprisingly &#8212; to the crowd, at least &#8212; avuncular mode (this is part one of six &#8212; <a href="http://allthingsd.com/20080630/the-entire-d6-interview-with-news-corps-rupert-murdoch-1-of-5/">here are the rest</a>):</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-AE658ADA_8C35_4B19_9E43_4C8BDF030CD0.html"></iframe></p>
<p><strong>D7 (2009)</strong></p>
<p>Twitter&#8217;s Biz Stone and Evan Williams in simpler days:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-ABE978B3_7782_4F48_A7F2_8CD121F47CFB.html"></iframe></p>
<p><strong>D8 (2010)</strong></p>
<p>Zuckerberg and the hoodie that saved the day:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-29CC1557_56A9_4484_90B4_539E282F6F9A.html"></iframe></p>
<p><strong>D9 (2011)</strong></p>
<p>Browser man and VC Marc Andreessen on software eating the world:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-9A42E2B2_7C7D_4085_9EEE_724C0B17D949.html"></iframe></p>
<p><strong>D10 (2012)</strong></p>
<p>Hollywood superagent Ari Emanuel is not shy:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-A7ACC8D5_A371_4BCD_A0F3_99D02A85A291.html"></iframe></p>
<p><strong>D11 (2013)</strong></p>
<p>Tesla and SpaceX founder Elon Musk <em>is</em> Tony Stark:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-1F7C3196_2B5D_4A1F_9FD0_B8DF95FFC97D.html"></iframe></p>
<p><strong>Dive Into Media (2012)</strong></p>
<p>Rust never sleeps for Neil Young:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-1598C8DC_7B17_4E42_A95A_DE703ACC12A9.html"></iframe></p>
<p><strong>Dive Into Media (2013)</strong></p>
<p>Vice&#8217;s Shane Smith and CollegeHumor&#8217;s Ricky Van Veen are also not shy:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-AF400E65_E3E6_4AD8_9A97_0BAF0C6759B4.html"></iframe></p>
<p><strong>Dive Into Mobile (2010)</strong></p>
<p>Google&#8217;s Susan Wojcicki is the most powerful Internet exec you don&#8217;t know as well as you should:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-134DF2D6_FEC3_4FD5_BEE9_DBFF6C3C0190.html"></iframe></p>
<p><strong>Dive Into Mobile (2013)</strong></p>
<p>Google&#8217;s Eric Schmidt will take your questions now:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-3C3DD190_BBFF_403E_B635_B5BC711E927A.html"></iframe></p>
<p><strong>AsiaD (2011)</strong></p>
<p>Jack Dorsey of Twitter and Square is very inventive:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-5C248BB9_1AF1_48FF_B4D7_80147AA09CB3.html"></iframe></p>
<p><strong>D@CES (2010)</strong></p>
<p>Netflix&#8217;s Reed Hastings knows video:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-3C83759C_62BC_4B22_A9AB_27333087510D.html"></iframe></p>
<p><strong>D@CES (2011)</strong></p>
<p>Twitter&#8217;s Dick Costolo is the fashion police of Las Vegas:</p>
<p><iframe frameborder="0" scrolling="no" width="512" height="288" src="http://live.wsj.com/public/page/embed-989DDABF_54D1_4900_995A_631291AB54CA.html"></iframe></p>
