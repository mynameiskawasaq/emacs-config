<p>
Last week, Bin Chen shared his workflow for <a href="http://blog.binchen.org/posts/how-to-accept-the-github-pull-request-efficiently.html">merging Github pull
requests</a>. Other than the use of Firefox instead of Conkeror, it was
identical to mine. Now I gladly come to admit that <a href="https://github.com/alexander-yakushev">Alexander Yakushev</a>
has outsmarted us both by fixing up <a href="https://github.com/sigma/magit-gh-pulls">magit-gh-pulls-mode</a>, a package
originally written by <a href="https://github.com/sigma">Yann Hodique</a> which does it all from within
Magit.
</p>

<p>
You can find the keybinds on the <a href="https://github.com/sigma/magit-gh-pulls#usage">Readme</a> page.
</p>

<p>
It bothers me very slightly that it immediately queries for
pull-requests when I first call <code>magit-status</code>, even if I’m not
interested in pull-requests at the moment. So we change the suggested
setup in order to fix that.
</p>
<figure class="highlight"><pre><code class="language-cl" data-lang="cl"><span class="p">(</span><span class="nb">when</span> <span class="p">(</span><span class="nb">fboundp</span> <span class="ss">'magit-gh-pulls-mode</span><span class="p">)</span>
  <span class="p">(</span><span class="nv">eval-after-load</span> <span class="ss">'magit</span>
    <span class="o">'</span><span class="p">(</span><span class="nv">define-key</span> <span class="nv">magit-mode-map</span> <span class="s">"#gg"</span>
       <span class="nf">#'</span><span class="nv">endless/load-gh-pulls-mode</span><span class="p">))</span>

  <span class="p">(</span><span class="nb">defun</span> <span class="nv">endless/load-gh-pulls-mode</span> <span class="p">()</span>
    <span class="s">"Start `magit-gh-pulls-mode' only after a manual request."</span>
    <span class="p">(</span><span class="nv">interactive</span><span class="p">)</span>
    <span class="p">(</span><span class="nb">require</span> <span class="ss">'magit-gh-pulls</span><span class="p">)</span>
    <span class="p">(</span><span class="nv">add-hook</span> <span class="ss">'magit-mode-hook</span> <span class="nf">#'</span><span class="nv">turn-on-magit-gh-pulls</span><span class="p">)</span>
    <span class="p">(</span><span class="nv">magit-gh-pulls-mode</span> <span class="mi">1</span><span class="p">)</span>
    <span class="p">(</span><span class="nv">magit-gh-pulls-reload</span><span class="p">)))</span></code></pre></figure>
<p>
With this setup it’ll only activate <code>magit-gh-pulls-mode</code> after you
try to update the list of pull-requests.
</p>

   <p><a href="http://endlessparentheses.com/merging-github-pull-requests-from-emacs.html?source=rss#disqus_thread">Comment on this.</a></p>