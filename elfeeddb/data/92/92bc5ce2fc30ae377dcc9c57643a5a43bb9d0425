<p>Recently, I've had to code some C++ at work. And I saw it as a good
opportunity to step up my Emacs' IDE game. I've eschewed clang-based
tools until now, but GCC isn't adding AST support any time soon, and
CEDET is too slow and too clumsy with macros for the particular
project that I
had. Here's
<a href="https://bitbucket.org/eigen/eigen/src/525f03452c37a400bcc4df194950fa97aa61936a/Eigen/src/Core/Matrix.h?at=default&amp;fileviewer=file-view-default#Matrix.h-426">the line</a> in
<a href="http://eigen.tuxfamily.org/index.php?title=Main_Page">Eigen</a> that
broke the camel's back. Basically it's 30 lines of macros that expand
to 30 lines of typedefs. Maybe it's a valid implementation choice, I'd
rather avoid the macros altogether, but in any case I couldn't get
CEDET to parse that.</p>

<h2 id="use-rtags-for-navigation">Use Rtags for navigation</h2>

<p>The first thing I tried
was <a href="https://github.com/Andersbakken/rtags">rtags</a>.  My project was
CMake-based, so I just put this line in my subdirectory <code>Makefile</code>:</p>
<div class="highlight"><pre><code class="language-text"><span></span>cmake:
    cd ../build &amp;&amp; cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ..
</code></pre></div>
<p>The <code>-DCMAKE_EXPORT_COMPILE_COMMANDS=1</code> causes a
<code>compile_commands.json</code> file to be emitted during the actual
compilation. This file describes the compile flags for every source
file. These flags are essential in helping the parser understand
what's going on.</p>

<p>Then, in the <code>build</code> directory I start:</p>
<div class="highlight"><pre><code class="language-text"><span></span>rdm &amp; jc -J .
</code></pre></div>
<p>Finally, <code>rtags-find-symbol-at-point</code> should work now. I still like to
use CEDET as backup, it's pretty good at tracking variables
defined in the current function:</p>
<div class="highlight"><pre><code class="language-elisp"><span></span><span class="p">(</span><span class="nb">defun</span> <span class="nv">ciao-goto-symbol</span> <span class="p">()</span>
  <span class="p">(</span><span class="k">interactive</span><span class="p">)</span>
  <span class="p">(</span><span class="nv">deactivate-mark</span><span class="p">)</span>
  <span class="p">(</span><span class="nv">ring-insert</span> <span class="nv">find-tag-marker-ring</span> <span class="p">(</span><span class="nf">point-marker</span><span class="p">))</span>
  <span class="p">(</span><span class="k">or</span> <span class="p">(</span><span class="k">and</span> <span class="p">(</span><span class="nb">require</span> <span class="ss">'rtags</span> <span class="no">nil</span> <span class="no">t</span><span class="p">)</span>
           <span class="p">(</span><span class="nv">rtags-find-symbol-at-point</span><span class="p">))</span>
      <span class="p">(</span><span class="k">and</span> <span class="p">(</span><span class="nb">require</span> <span class="ss">'semantic/ia</span><span class="p">)</span>
           <span class="p">(</span><span class="k">condition-case</span> <span class="no">nil</span>
               <span class="p">(</span><span class="nv">semantic-ia-fast-jump</span> <span class="p">(</span><span class="nf">point</span><span class="p">))</span>
             <span class="p">(</span><span class="ne">error</span> <span class="no">nil</span><span class="p">)))))</span>
<span class="p">(</span><span class="nf">define-key</span> <span class="nv">c++-mode-map</span> <span class="p">(</span><span class="nv">kbd</span> <span class="s">"M-."</span><span class="p">)</span> <span class="ss">'ciao-goto-symbol</span><span class="p">)</span>
<span class="p">(</span><span class="nf">define-key</span> <span class="nv">c++-mode-map</span> <span class="p">(</span><span class="nv">kbd</span> <span class="s">"M-,"</span><span class="p">)</span> <span class="ss">'pop-tag-mark</span><span class="p">)</span>
</code></pre></div>
<p>For my other C++ projects which aren't CMake-based, I use the
excellent <a href="https://github.com/rizsotto/Bear">bear</a> tool to emit the
<code>compile_commands.json</code> file. It's as easy as:</p>
<div class="highlight"><pre><code class="language-sh"><span></span>make clean
bear make
</code></pre></div>
<h2 id="use-irony-for-completion">Use Irony for completion</h2>

<p>It didn't take long to figure out that <code>rtags</code> isn't great at
completion. I almost accepted that's just the way it is.  But this
morning I decided to make some changes and
try <a href="https://github.com/Sarcasm/irony-mode">irony-mode</a>.  And it
worked beautifully for completion! What's ironic, is that <code>irony-mode</code>
doesn't have <code>goto-symbol</code>, so the time spent to figure out <code>rtags</code>
was worth it.</p>

<p>Here's my Irony setup; I only changed the <kbd>C-M-i</kbd> binding to
the newly written <code>counsel-irony</code>, now available in the <code>counsel</code>
package on MELPA:</p>
<div class="highlight"><pre><code class="language-elisp"><span></span><span class="p">(</span><span class="nv">add-hook</span> <span class="ss">'c++-mode-hook</span> <span class="ss">'irony-mode</span><span class="p">)</span>
<span class="p">(</span><span class="nv">add-hook</span> <span class="ss">'c-mode-hook</span> <span class="ss">'irony-mode</span><span class="p">)</span>

<span class="p">(</span><span class="nb">defun</span> <span class="nv">my-irony-mode-hook</span> <span class="p">()</span>
  <span class="p">(</span><span class="nf">define-key</span> <span class="nv">irony-mode-map</span>
      <span class="p">[</span><span class="nv">remap</span> <span class="nv">completion-at-point</span><span class="p">]</span> <span class="ss">'counsel-irony</span><span class="p">)</span>
  <span class="p">(</span><span class="nf">define-key</span> <span class="nv">irony-mode-map</span>
      <span class="p">[</span><span class="nv">remap</span> <span class="nv">complete-symbol</span><span class="p">]</span> <span class="ss">'counsel-irony</span><span class="p">))</span>
<span class="p">(</span><span class="nv">add-hook</span> <span class="ss">'irony-mode-hook</span> <span class="ss">'my-irony-mode-hook</span><span class="p">)</span>
<span class="p">(</span><span class="nv">add-hook</span> <span class="ss">'irony-mode-hook</span> <span class="ss">'irony-cdb-autosetup-compile-options</span><span class="p">)</span>
</code></pre></div>
<p>And here are some screenshots of <code>counsel-irony</code>:</p>

<p><img alt="screenshot-1" src="http://oremacs.com/download/counsel-irony-1.png" /></p>

<p>First of all, the completion is displayed inline, similarly to modern IDEs.
You can use all of Ivy's regex tricks to complete your candidate:</p>

<p><img alt="screenshot-2" src="http://oremacs.com/download/counsel-irony-2.png" /></p>

<p>Note how the power of regex matching allows me to narrow the initial
1622 candidates to only 22 functions that have <code>src1</code> and <code>src2</code> as
arguments. One of the candidates is cut off for being longer than the
window width. You can still match against the invisible text, but you
won't see it. It's possible to use <kbd>C-c C-o</kbd> (<code>ivy-occur</code>) to
store the current candidates into a buffer:</p>

<p><img alt="screenshot-3" src="http://oremacs.com/download/counsel-irony-3.png" /></p>

<p>Clicking the mouse on any of the lines in the new buffer will insert
the appropriate symbol into the C++ buffer.</p>

<h2 id="outro">Outro</h2>

<p>I'd like to thank the authors of <code>rtags</code> and <code>irony-mode</code> for these
nice packages. Hopefully, <code>counsel-irony</code> is a nice addition. Happy
hacking!</p>