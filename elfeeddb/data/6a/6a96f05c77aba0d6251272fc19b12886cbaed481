<p><a href="http://blog.ionic.io/ionic-3-0-has-arrived/">Ionic 3.0 has arrived!</a> It was time to update the DreamHouse sample application. In this post, I share a new version of DreamHouse built with Ionic 3 and Angular 4.</p>
<p>Watch the video:</p>
<p><iframe width="640" height="360" src="https://www.youtube.com/embed/viik6jXsuqk?ecver=1" frameborder="0" allowfullscreen></iframe></p>
<h2>Source Code</h2>
<p>The source code and installation instructions are available in <a href="https://github.com/dreamhouseapp/dreamhouse-mobile-ionic">this repository</a>.</p>
<h2>3-Minute Installation</h2>
<ol>
<li>Make sure you have the latest version of Cordova and Ionic:
<pre class="brush: bash; title: ; notranslate">
npm install -g cordova
npm install -g ionic
</pre>
</li>
<li>
Clone the repository:</p>
<pre class="brush: bash; title: ; notranslate">
git clone https://github.com/dreamhouseapp/dreamhouse-mobile-ionic
</pre>
</li>
<li>
Navigate to the dreamhouse-mobile-ionic directory :</p>
<pre class="brush: bash; title: ; notranslate">
cd dreamhouse-mobile-ionic
</pre>
</li>
<li>
Install the dependencies</p>
<pre class="brush: bash; title: ; notranslate">
npm install
</pre>
</li>
<li>
Run the app</p>
<pre class="brush: bash; title: ; notranslate">
ionic serve
</pre>
</li>
</ol>
<h2>Mock and REST Services</h2>
<p>The app comes with two interchangeable implementations of the data services:</p>
<ul>
<li>The <strong>Mock services</strong> implementation uses in-memory data and are used for prototyping or testing purpose.</li>
<li>The <strong>REST services</strong> implementation uses Angular’s new observable-based http object to access Node.js-based REST services.</li>
</ul>
<p>The mock services implementation is used by default. Follow the instructions in the <a href="http://<a href="https://github.com/dreamhouseapp/dreamhouse-mobile-ionic">repository readme file</a> to switch to the REST services implementation.</p>
<p><img src="http://coenraets.org/blog/wp-content/uploads/2016/12/ionic2-salesforce.png" width="800"/></p>
