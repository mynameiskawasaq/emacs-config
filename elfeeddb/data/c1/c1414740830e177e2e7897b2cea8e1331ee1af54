<div class="paragraph">
<p>On behalf of the community I&#8217;m pleased to announce the release of <a href="http://docs.spring.io/spring-session/docs/2.0.0.M1/reference/html5/">Spring Session 2.0.0.M1</a>. This release is focused primarily on ensuring compatability with Spring Framework 5 which is the minimum Spring version required.</p>
</div>
<h1 id="supported-data-stores" class="sect0"><a class="anchor" href="#supported-data-stores"></a>Supported Data Stores</h1>
<div class="paragraph">
<p>We have also removed some of the Spring Session implementations from the main repository. The goal is to allow the core Spring Session team to focus on delivering new features rather than needing to know the ins and outs of every data store. This will allow development of other modules to be done without the overhead of reviews from the Spring Session team.</p>
</div>
<div class="paragraph">
<p>Going forward,the Spring team will officially maintain Redis, JDBC, Hazelcast, &amp; Geode / GemFire implementations of Spring Session. These choices were made based upon the Spring Session teams expertise and popularity within Spring Session.</p>
</div>
<div class="paragraph">
<p>We will provide links to additional implementations that are maintained by the community. If you are interested in picking up support for another data store please get in touch in an <a href="https://github.com/spring-projects/spring-session/issues">issue</a>.</p>
</div>
<h1 id="feedback-please" class="sect0"><a class="anchor" href="#feedback-please"></a>Feedback Please</h1>
<div class="paragraph">
<p>If you have feedback on this release, I encourage you to reach out via <a href="http://stackoverflow.com/questions/tagged/spring-session">StackOverflow</a>, <a href="https://github.com/spring-projects/spring-session/issues">GitHub Issues</a>, or via the comments section. You can also ping me <a href="https://twitter.com/rob_winch">@rob_winch</a>, Joe <a href="https://twitter.com/joe_grandja">@joe_grandja</a>, or <a href="https://twitter.com/vedran_pavic">@vedran_pavic</a>  on Twitter.</p>
</div>
<div class="paragraph">
<p>Of course the best feedback comes in the form of <a href="https://github.com/spring-projects/spring-session/blob/2.0.0.M1/CONTRIBUTING.adoc">contributions</a>.</p>
</div>
<div class="paragraph">
<p><a href="http://projects.spring.io/spring-session/">Project Site</a> | <a href="http://docs.spring.io/spring-session/docs/2.0.0.M1/reference/html5/">Reference</a> | <a href="http://stackoverflow.com/questions/tagged/spring-session">Help</a></p>
</div>