<p>
<code>let-alist</code> is the best thing to happen to associative lists since the
invention of the cons cell. This little macro lets you easily access the
contents of an alist, concisely and efficiently, without having to specify them
preemptively. It comes built-in with 25.1, and is also available on GNU Elpa for
older Emacsen.
</p>

<p>
If you've ever had to process the output of a web API, you've certainly had to
deal with endless alists returned by <a href="http://doc.endlessparentheses.com/Fun/json-read"><code>json-read</code></a>. I'll spare you the rant and go
straight to the example. 
</p>

<p>
Here's a very simplified version of a function of the <a href="https://github.com/vermiculus/sx.el">SX</a> package, before <code>let-alist</code>.
</p>
<figure class="highlight"><pre><code class="language-cl" data-lang="cl"><span class="p">(</span><span class="nb">defun</span> <span class="nv">sx-question-list--print-info</span> <span class="p">(</span><span class="nv">question-data</span><span class="p">)</span>
  <span class="s">"DOC"</span>
  <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">tags</span>               <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'tags</span>               <span class="nv">question-data</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">answer_count</span>       <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'answer_count</span>       <span class="nv">question-data</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">title</span>              <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'title</span>              <span class="nv">question-data</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">last_activity_date</span> <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'last_activity_date</span> <span class="nv">question-data</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">score</span>              <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'score</span>              <span class="nv">question-data</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">owner-name</span> <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'display_name</span> <span class="p">(</span><span class="nb">cdr</span> <span class="p">(</span><span class="nv">assq</span> <span class="ss">'owner</span> <span class="nv">question-data</span><span class="p">))))))</span>
    <span class="p">(</span><span class="nb">list</span>
     <span class="nv">question-data</span>
     <span class="p">(</span><span class="nb">vector</span>
      <span class="p">(</span><span class="nv">int-to-string</span> <span class="nv">score</span><span class="p">)</span>
      <span class="p">(</span><span class="nv">int-to-string</span> <span class="nv">answer_count</span><span class="p">)</span>
      <span class="nv">title</span> <span class="s">"     "</span>
      <span class="nv">owner-name</span>
      <span class="nv">last_activity_date</span> 
      <span class="nv">sx-question-list-ago-string</span>
      <span class="s">" "</span> <span class="nv">tags</span><span class="p">))))</span></code></pre></figure>

<p>
And this is what the same function looks like now (again, simplified).
</p>
<figure class="highlight"><pre><code class="language-cl" data-lang="cl"><span class="p">(</span><span class="nb">defun</span> <span class="nv">sx-question-list--print-info</span> <span class="p">(</span><span class="nv">question-data</span><span class="p">)</span>
  <span class="s">"DOC"</span>
  <span class="p">(</span><span class="nv">let-alist</span> <span class="nv">question-data</span>
    <span class="p">(</span><span class="nb">list</span>
     <span class="nv">question-data</span>
     <span class="p">(</span><span class="nb">vector</span>
      <span class="p">(</span><span class="nv">int-to-string</span> <span class="o">.</span><span class="nv">score</span><span class="p">)</span>
      <span class="p">(</span><span class="nv">int-to-string</span> <span class="o">.</span><span class="nv">answer_count</span><span class="p">)</span>
      <span class="o">.</span><span class="nv">title</span> <span class="s">"     "</span>
      <span class="o">.</span><span class="nv">owner.display_name</span>
      <span class="o">.</span><span class="nv">last_activity_date</span> <span class="nv">sx-question-list-ago-string</span>
      <span class="s">" "</span> <span class="o">.</span><span class="nv">tags</span><span class="p">))))</span></code></pre></figure>
<p>
How much nicer is that? <code>let-alist</code> detects all those symbols that start with a
<code>.</code>, and wraps the body in a <code>let</code> form essentially identical to the one above.
The resulting code is much nicer to write, and the byte-compiled result is
exactly as efficient as the manually written version. (If it's not
byte-compiled, there will be a performance difference, though it should be
small.)
</p>

<p>
And just to make things nicer, you can use this snippet to highlight those <code>.</code>
symbols as if they were keywords.
</p>
<figure class="highlight"><pre><code class="language-cl" data-lang="cl"><span class="p">(</span><span class="nv">font-lock-add-keywords</span>
 <span class="ss">'emacs-lisp-mode</span>
 <span class="o">'</span><span class="p">((</span><span class="s">"\\_&lt;\\.\\(?:\\sw\\|\\s_\\)+\\_&gt;"</span> <span class="mi">0</span> 
   <span class="nv">font-lock-builtin-face</span><span class="p">)))</span></code></pre></figure>

<div id="outline-container-sec-1" class="outline-2">
<h2 id="sec-1">Update <span class="timestamp-wrapper"><span class="timestamp">&lt;2014-12-20 Sat&gt;</span></span></h2>
<div class="outline-text-2" id="text-1">
<p>
Due to popular demand, <code>let-alist</code> now does nested alists. The example above
shows how you can use <code>.owner.display_name</code> to access the value of
<code>display_name</code> inside the value of <code>owner</code>.</p>
</div>
</div>

   <p><a href="http://endlessparentheses.com/new-on-elpa-and-in-emacs-25-1-let-alist.html?source=rss#disqus_thread">Comment on this.</a></p>