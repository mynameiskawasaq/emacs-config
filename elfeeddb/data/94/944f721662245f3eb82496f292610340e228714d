<p><span style="color:#5a5a5a;"><em>Now that the unnecessary headers have been removed, it&#8217;s time for Phase 2: How can you limit dependencies on the internals of a class?<br />
</em></span></p>
<p>
 </p>
<h1>Problem<br />
</h1>
<h2>JG Questions<br />
</h2>
<p>1. What does <span style="color:#2e74b5;">private</span> mean for a class member in C++?
</p>
<p>2. Why does changing the private members of a type cause a recompilation?
</p>
<h2>Guru Question<br />
</h2>
<p>3. Below is how the header from the previous Item looks after the initial cleanup pass. What further <span style="color:#2e74b5;">#include</span>s could be removed if we made some suitable changes, and how?
</p>
<p>This time, you may make changes to <span style="color:#2e74b5;">X</span> as long as <span style="color:#2e74b5;">X</span>&#8216;s base classes and its public interface remain unchanged; any current code that already uses <span style="color:#2e74b5;">X</span> should not be affected beyond requiring a simple recompilation.
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>//  x.h: sans gratuitous headers<br />//<br />#include &lt;iosfwd&gt;<br />#include &lt;list&gt;<br /><br />// None of A, B, C, or D are templates.<br />// Only A and C have virtual functions.<br />#include "a.h"  // class A<br />#include "b.h"  // class B<br />#include "c.h"  // class C<br />#include "d.h"  // class D<br />class E;<br /><br />class X : public A, private B {<br />public:<br />       X( const C&amp; );<br />    B  f( int, char* );<br />    C  f( int, C );<br />    C&amp; g( B );<br />    E  h( E );<br />    virtual std::ostream&amp; print( std::ostream&amp; ) const;<br /><br />  private:<br />    std::list&lt;C&gt; clist;<br />    D            d;<br />};<br /><br />std::ostream&amp; operator&lt;&lt;( std::ostream&amp; os, const X&amp; x ) {<br />    return x.print(os);<br />}
</code></pre>
</p>
<p>
 </p>
<h1>Solution<br />
</h1>
<h2>1. What does private mean for a class member in C++?<br />
</h2>
<p>It means that outside code cannot access that member. Specifically, it cannot name it or call it.
</p>
<p>For example, given this class:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>class widget {<br />public:<br />    void f() { }<br />private:<br />    void f(int) { }<br />    int i;<br />};
</code></pre>
</p>
<p>Outside code cannot use the name of the <span style="color:#2e74b5;">private</span> members:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code> int main() {<br />    auto w = widget{};<br />    w.f();               // ok<br />    w.f(42);             // error, cannot access name "f(int)"<br />    w.i = 42;            // error, cannot access name "i"<br />}
</code></pre>
</p>
<p>
 </p>
<h2>2. Why does changing the private members of a type cause a recompilation?<br />
</h2>
<p>Because private data members can change the size of the object, and private member functions participate in overload resolution.
</p>
<p>Note that accessibility is still safely enforced: <em>Calling code</em> still doesn&#8217;t get to use the private parts of the class. However, <em>the compiler</em> gets to know all about them at all times, including as it compiles the calling code. This does increase build coupling, but it&#8217;s for a deliberate reason: C++ has always been designed for efficiency, and a little-appreciated cornerstone of that is that C++ is designed to by default expose a type&#8217;s full implementation to the compiler in order to make aggressive optimization easier. It&#8217;s one of the fundamental reasons C++ is an efficient language.
</p>
<p>
 </p>
<h2>3. What further #includes could be removed if we made some suitable changes, and how? … any current code that already uses X should not be affected beyond requiring a simple recompilation.<br />
</h2>
<p>There are a few things we weren&#8217;t able to do in the previous problem:
</p>
<ul>
<li>We had to leave <span style="color:#2e74b5;">a.h</span> and <span style="color:#2e74b5;">b.h</span>. We couldn&#8217;t get rid of these because <span style="color:#2e74b5;">X</span> inherits from both <span style="color:#2e74b5;">A</span> and <span style="color:#2e74b5;">B</span>, and you always have to have full definitions for base classes so that the compiler can determine <span style="color:#2e74b5;">X</span>&#8216;s object size, virtual functions, and other fundamentals. (Can you anticipate how to remove one of these? Think about it: Which one can you remove, and why/how? The answer will come shortly.)
</li>
<li>We had to leave <span style="color:#2e74b5;">list</span>, <span style="color:#2e74b5;">c.h</span> and <span style="color:#2e74b5;">d.h</span>. We couldn&#8217;t get rid of these right away because a <span style="color:#2e74b5;">list&lt;C&gt;</span> and a <span style="color:#2e74b5;">D</span> appear as private data members of <span style="color:#2e74b5;">X</span>. Although <span style="color:#2e74b5;">C</span> appears as neither a base class nor a member, it is being used to instantiate the <span style="color:#2e74b5;">list</span> member, and some have compilers required that when you instantiate <span style="color:#2e74b5;">list&lt;C&gt;</span> you be able to see the definition of <span style="color:#2e74b5;">C</span>. (The standard doesn&#8217;t require a definition here, though, so even if the compiler you are currently using has this restriction, you can expect the restriction to go away over time.)
</li>
</ul>
<p>Now let&#8217;s talk about the beauty of Pimpls.
</p>
<p>
 </p>
<h3>The Pimpl Idiom<br />
</h3>
<p>C++ lets us easily encapsulate the private parts of a class from unauthorized access. Unfortunately, because of the header file approach inherited from C, it can take a little more work to encapsulate dependencies on a class&#8217; privates.
</p>
<p>&#8220;But,&#8221; you say, &#8220;the whole point of encapsulation is that the client code shouldn&#8217;t have to know or care about a class&#8217; private implementation details, right?&#8221; Right, and in C++ the client code doesn&#8217;t need to know or care about access to a class&#8217; privates (because unless it&#8217;s a friend it isn&#8217;t allowed any), but because the privates are visible in the header the client code does have to depend upon any types they mention. This coupling between the caller and the class&#8217;s internal details creates dependencies on both (re)compilation and binary layout.
</p>
<p>How can we better insulate clients from a class&#8217; private implementation details? One good way is to use a special form of the handle/body idiom, popularly called the Pimpl Idiom because of the intentionally pronounceable <span style="color:#2e74b5;">pimpl</span> pointer, as a compilation firewall.
</p>
<p>A Pimpl is just an opaque pointer (a pointer to a forward-declared, but undefined, helper class) used to hide the private members of a class. That is, instead of writing this:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>// file widget.h<br />//<br />class widget {<br />    // public and protected members<br />private:<br />    // private members; whenever these change,<br />    // all client code must be recompiled<br />};
</code></pre>
</p>
<p>We write instead:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>// file widget.h<br />//<br />#include &lt;memory&gt;<br /><br />class widget {<br />public:<br />    widget();<br />    ~widget();<br />    // public and protected members<br />private:<br />    struct impl;<br />    std::unique_ptr&lt;impl&gt; pimpl;   // ptr to a forward-declared class<br />};<br /><br />// file widget.cpp<br />//<br />#include "widget.h"<br /><br />struct widget::impl {<br />    // private members; fully hidden, can be<br />    // changed at will without recompiling clients<br />};<br /><br />widget::widget() : pimpl{ make_unique&lt;widget::impl&gt;(/*...*/) } { }<br />widget::~widget() =default;
</code></pre>
</p>
<p>Every <span style="color:#2e74b5;">widget</span> object dynamically allocates its <span style="color:#2e74b5;">impl</span> object. If you think of an object as a physical block, we&#8217;ve essentially lopped off a large chunk of the block and in its place left only &#8220;a little bump on the side&#8221;—the opaque pointer, or Pimpl. If copy and move are appropriate for your type, write those four operations to perform a deep copy that clones the <span style="color:#2e74b5;">impl</span> state.
</p>
<p>The major advantages of this idiom come from the fact that it breaks the caller&#8217;s dependency on the private details, including breaking both compile-time dependencies and binary dependencies:
</p>
<ul>
<li>Types mentioned only in a class&#8217; implementation need no longer be defined for client code, which can eliminate extra <span style="color:#2e74b5;">#include</span>s and improve compile speeds.
</li>
<li>A class&#8217; implementation can be changed—that is, private members can be freely added or removed—without recompiling client code. This is a useful technique for providing ABI-safety or binary compatibility, so that the client code is not dependent on the exact layout of the object.
</li>
</ul>
<p>The major costs of this idiom are in performance:
</p>
<ul>
<li>Each construction/destruction must allocate/deallocate memory.
</li>
<li>Each access of a hidden member can require at least one extra indirection. (If the hidden member being accessed itself uses a back pointer to call a function in the visible class, there will be multiple indirections, but is usually easy to avoid needing a back pointer.)
</li>
</ul>
<p>And of course we&#8217;re replacing any removed headers with the <span style="color:#2e74b5;">&lt;memory&gt;</span> header.
</p>
<p>We&#8217;ll come back to these and other Pimpl issues in GotW #24. For now, in our example, there were three headers whose definitions were needed simply because they appeared as private members of <span style="color:#2e74b5;">X</span>. If we instead restructure <span style="color:#2e74b5;">X</span> to use a Pimpl, we can immediately make several further simplifications:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>#include &lt;list&gt;<br />#include "c.h"  // class C<br />#include "d.h"  // class D
</code></pre>
</p>
<p>One of these headers (<span style="color:#2e74b5;">c.h</span>) can be replaced with a forward declaration because <span style="color:#2e74b5;">C</span> is still being mentioned elsewhere as a parameter or return type, and the other two (<span style="color:#2e74b5;">list</span> and <span style="color:#2e74b5;">d.h</span>) can disappear completely.
</p>
<blockquote>
<p><strong>Guideline:</strong> For widely-included classes whose implementations may change, or to provide ABI-safety or binary compatibility, consider using the compiler-firewall idiom (Pimpl Idiom) to hide implementation details. Use an opaque pointer (a pointer to a declared but undefined class) declared as <strong>struct impl; std::unique_ptr&lt;impl&gt; pimpl;</strong> to store private nonvirtual members.
</p>
</blockquote>
<p>
 </p>
<p>Note: We can&#8217;t tell from the original code by itself whether or not <span style="color:#2e74b5;">X</span> had (default) copy or move operations. If it did, then to preserve that we would need to write them again ourselves since the move-only <span style="color:#2e74b5;">unique_ptr</span> member suppresses the implicit generation of copy construction and copy assignment, and the user-declared destructor suppresses the implicit generation of move construction and move assignment. If we do need to write them by hand, the move constructor and move assignment can be <span style="color:#2e74b5;">=default</span>ed, and the copy constructor and copy assignment will need to copy the Pimpl object.
</p>
<p>After making that additional change, the header looks like this:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>//  x.h: after converting to use a Pimpl<br />//<br />#include &lt;iosfwd&gt;<br />#include &lt;memory&gt;<br />#include "a.h"  // class A (has virtual functions)<br />#include "b.h"  // class B (has no virtual functions)<br />class C;<br />class E;<br /><br />class X : public A, private B {<br />public:<br />    ~X();                          // defined out of line<br />    // and copy/move operations if X had them before<br /><br />       X( const C&amp; );<br />    B  f( int, char* );<br />    C  f( int, C );<br />    C&amp; g( B );<br />    E  h( E );<br />    virtual std::ostream&amp; print( std::ostream&amp; ) const;<br /><br />private:<br />    struct impl;<br />    std::unique_ptr&lt;impl&gt; pimpl;   // ptr to a forward-declared class<br />};<br /><br />std::ostream&amp; operator&lt;&lt;( std::ostream&amp; os, const X&amp; x ) {<br />    return x.print(os);<br />}
</code></pre>
</p>
<p>Without more extensive changes, we still need the definitions for <span style="color:#2e74b5;">A</span> and <span style="color:#2e74b5;">B</span> because they are base classes, and we have to know at least their sizes in order to define the derived class <span style="color:#2e74b5;">X</span>.
</p>
<p>The private details go into <span style="color:#2e74b5;">X</span>&#8216;s implementation file where client code never sees them and therefore never depends upon them:
</p>
<p style="background:#ffffcc;margin-left:14pt;">
<pre><code>//  Implementation file x.cpp<br />//<br />#include &lt;list&gt;<br />#include "c.h"  // class C<br />#include "d.h"  // class D<br />using namespace std;<br /><br />struct X::impl {<br />    list&lt;C&gt; clist;<br />    D       d;<br />};<br /><br />X::X() : pimpl{ make_unique&lt;X::impl&gt;(/*...*/) } { }<br />X::~X() =default;
</code></pre>
</p>
<p>That brings us down to including only four headers, which is a great improvement—but it turns out that there is still a little more we could do, if only we were allowed to change the structure of <span style="color:#2e74b5;">X</span> more extensively. This leads us nicely into Part 3…
</p>
<p>
 </p>
<h2>Acknowledgments<br />
</h2>
<p>Thanks to the following for their feedback to improve this article: John Humphrey, thokra, Motti Lanzkron, Marcelo Pinto.</p><br />Filed under: <a href='https://herbsutter.com/category/c/gotw/'>GotW</a>  <img alt="" border="0" src="https://pixel.wp.com/b.gif?host=herbsutter.com&#038;blog=3379246&#038;post=2384&#038;subd=herbsutter&#038;ref=&#038;feed=1" width="1" height="1" />