<p>Yesterday I <a href="https://herbsutter.com/2014/05/20/cppcon-my-proposed-talks-part-1/">posted three</a> of my proposed talks for CppCon. These are the ones I&#8217;ve given publicly before, but they&#8217;re not retreads – all are fresh and up to date, with refreshed or new material.
</p>
<p>But I&#8217;ve also proposed two brand new talks – titles and abstracts are below.
</p>
<blockquote>
<p>Note: The CppCon program committee will be considering the talk proposals to come up with a balanced program, so it&#8217;s possible that not all of these will be in the final program, but I have high hopes for most of these…
</p>
</blockquote>
<p>CppCon is building well. I don&#8217;t know how many talks there will be in the end as this depends on the program committee including in part how long the accepted talks are, but FWIW my talks are submissions #41 and #126-129. There&#8217;s some really great stuff in the pipeline on all sorts of topics of interest to C++ developers, not just about the language itself but also tools, specific domains, and lots of cool stuff that will be shown for the first time at CppCon.
</p>
<p>I look forward to seeing many of you at CppCon this fall! If we have the right equipment in the main auditorium, there might even be a wind of change…
</p>
<p style="text-align:center;">
 </p>
<h2>GC for C++, and C++ for GC: &#8220;Right&#8221; and &#8220;Wrong&#8221; Ways to Add Garbage Collection to C++ (1 to 2 hours)<br />
</h2>
<p><span style="font-family:Verdana;font-size:10pt;">&#8220;Garbage collection is essential to modern programming!&#8221; &#8220;Garbage collection is silly, who needs it!&#8221;<br />
</span></p>
<p><span style="font-family:Verdana;font-size:10pt;">As is usual with extremes, both of these statements are wrong. Garbage collection (GC) is not needed for most C++ programs; we&#8217;re very happy with determinism and smart pointers, and GC is absolutely penalizing when overused, especially as a default (or, <em>shudder</em>, only) memory allocator. However, the truth is that GC is also important to certain high-performance and highly-concurrent data structures, because it helps solve advanced lock-free problems like the ABA problem better than workarounds like hazard pointers.<br />
</span></p>
<p><span style="font-family:Verdana;font-size:10pt;">This talk presents thoughts about how GC can be added well to C++, directly complementing (not competing with) C++&#8217;s existing strengths and demonstrating why, as Stroustrup says, &#8220;C++ is the best language for garbage collection.&#8221;</span>
	</p>
<p>
 </p>
<h2>Addressing C++&#8217;s #1 Problem: Defining a C++ ABI (1 hour)<br />
</h2>
<p>&#8220;Why can&#8217;t I share C++ libraries even between my own internal teams without using the identical compiler and switch settings?&#8221; &#8220;Why are operating system APIs written in unsafe C, instead of C++?&#8221; &#8220;Why can&#8217;t I use <em>std::string</em> in a public shared library interface; it&#8217;s the C++ string, isn&#8217;t it?!&#8221;
</p>
<p>These and more perennial questions are caused by the same underlying problem: For various historical reasons, C++ does not have a standard binary interface, or ABI. Partial solutions exist, from the Itanium ABI which addresses only the language and only on some platforms, to COM and CORBA which do both less and far more than is needed.
</p>
<p>It is deeply ironic that there actually is a way to write an API in C++ so that it has a de facto stable binary ABI on every platform: <em>extern &#8220;C&#8221;</em>.
</p>
<p>This session describes current work driven by the presenter to develop a standard C++ ABI. This does not mean having identical binaries on all platforms. It does mean tackling all of the above problems, including directly addressing the #1 valid reason to use C instead of C++, and removing a major obstacle to sharing binary C++ libraries in a modern way.</p><br />Filed under: <a href='https://herbsutter.com/category/c/'>C++</a>  <img alt="" border="0" src="https://pixel.wp.com/b.gif?host=herbsutter.com&#038;blog=3379246&#038;post=2493&#038;subd=herbsutter&#038;ref=&#038;feed=1" width="1" height="1" />