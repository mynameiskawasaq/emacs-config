<p>I <a href="http://coenraets.org/blog/2017/01/react-native-sample-app-tutorial/">recently shared</a> a sample Employee Directory / Org Chart application built with <a href="https://facebook.github.io/react-native/">React Native</a>. That version of the application used a Node.js back-end. In this post, I’ll share the same application powered by a Salesforce back-end and built with the <a href="https://developer.salesforce.com/page/Mobile_SDK">Salesforce Mobile SDK</a>. Salesforce automatically provides the org chart data for users (employees) and contacts, so it makes a lot of sense to build this type of app on the Salesforce platform. The Salesforce Mobile SDK provides a series of services that make it easy to build React Native apps that integrate with Salesforce: authentication, Salesforce REST services library, push notification, encrypted database, offline synchronization, etc.</p>
<p>Watch the video to see the Employee Directory application in action:</p>
<p><iframe width="640" height="360" src="https://www.youtube.com/embed/2Pbgi2d6hEw" frameborder="0" allowfullscreen></iframe></p>
<p>Follow the steps below to install and run the application on your system. These instructions assume you are developing an iOS app on a Mac.</p>
<h3>Step 1: Install React Native</h3>
<p>Follow the React Native <a href="https://facebook.github.io/react-native/docs/getting-started.html">getting started instructions</a> to install React Native. Make sure you select your Mobile OS and Development OS for the right instructions. </p>
<h3>Step 2: Install the Salesforce Mobile SDK</h3>
<ol>
<li>On the command prompt, type the following command to install <a href="https://cocoapods.org/">CocoaPods</a>:
<pre class="brush: bash; title: ; notranslate">
sudo gem install cocoapods
</pre>
<p>CocoaPods is a dependency manager for Swift and Objective-C used by the Salesforce Mobile SDK to merge Mobile SDK modules into Xcode projects.
</li>
<li>Type the following command to install the Salesforce Mobile SDK for iOS:
<pre class="brush: bash; title: ; notranslate">
sudo npm install -g forceios
</pre>
</li>
</ol>
<h3>Step 3: Create the Project</h3>
<ol>
<li>Open a command prompt and type the following command to create a new React Native application using the Salesforce Mobile SDK:
<pre class="brush: bash; title: ; notranslate">
forceios create
</pre>
<p>Answer the prompts as follows (adjust the package name and organization name as needed):</p>
<div style="font-family: menlo,consolas,monospace;font-size:14px;padding-bottom:16px">
Enter your application type (native, native_swift, react_native, hybrid_local, hybrid_remote): <strong>react_native</strong><br />
Enter your application name: <strong>SalesforceEmployeeDirectory</strong><br />
Enter the package name for your app (com.mycompany.myapp): <strong>com.mycompany.orgchart</strong><br />
Enter your organization name (Acme, Inc.): <strong>MyCompany, Inc</strong><br />
Enter output directory for your app (leave empty for the current directory):
</div>
</li>
<li>Navigate (cd) to the project directory:
<pre class="brush: bash; title: ; notranslate">
cd SalesforceEmployeeDirectory
</pre>
</li>
<li>Open a command prompt and type the following command to start the local server:
<pre class="brush: bash; title: ; notranslate">
npm start
</pre>
</li>
<li>Open another command prompt and type the following command to run the app in the emulator:
<pre class="brush: bash; title: ; notranslate">
react-native run-ios
</pre>
</li>
</ol>
<h3>Step 4: Import Sample Data</h3>
<ol>
<li><a href="http://developer.salesforce.com/signup">Sign up</a> for a new Developer Edition.</li>
<li>Click <a href="https://login.salesforce.com/packaging/installPackage.apexp?p0=04t41000002OcpG">this link</a> to install the OrgChart unmanaged package into your org. The package adds an <strong>ImportOrgChartData</strong> Visualforce page and two fields (<strong>Picture__c</strong> and <strong>Include_In_Org_Chart__c</strong>) to the Contact object.</li>
<li>In the Developer Console, open the <strong>ImportOrgChartData</strong> Visualforce page. Click the <strong>Preview</strong> button, and click the <strong>Import Data</strong> button.</li>
</ol>
<div class="woo-sc-box  normal   ">The sample application works with the <strong>Contact</strong> object to allow you to load a meaningful number of employees using a Developer Edition (You can&#8217;t load as many records in the User object in a Developer Edition). You can easily modify the application queries to work on the <strong>User</strong> object in a real life environment.</div>
<h3>Step 4: Implement the Project</h3>
<ol>
<li>Clone the <a href="https://github.com/ccoenraets/employee-directory-react-native-salesforce">sample app repository</a>:
<pre class="brush: bash; title: ; notranslate">git clone https://github.com/ccoenraets/employee-directory-react-native-salesforce</pre>
</li>
<li>Copy the <strong>app</strong> folder from <strong>employee-directory-react-native-salesforce</strong> into your <strong>SalesforceEmployeeDirectory</strong> project folder</li>
<li>Open <strong>index.ios.js</strong>. Delete the existing content and replace it with:
<pre class="brush: jscript; title: ; notranslate">
import {AppRegistry} from 'react-native';

import EmployeeDirectoryApp from './app/EmployeeDirectoryApp';

AppRegistry.registerComponent('SalesforceEmployeeDirectory', 
                                  () =&gt; EmployeeDirectoryApp);
</pre>
</li>
<li>Hit <strong>Cmd + R</strong> to refresh the app in the emulator</li>
</ol>
<h3>Code Highlights</h3>
<p>There are two parts to the Salesforce integration in this application:</p>
<ul>
<li><strong>Authentication</strong>: The <strong>auth</strong> object of the <strong>react-native-force</strong> module makes it easy to authenticate with Salesforce using OAuth. Check out the <a href="https://github.com/ccoenraets/employee-directory-react-native-salesforce/blob/master/app/EmployeeDirectoryApp.js#L13-L20">EmployeeDirectoryApp</a> class for details.</li>
<li><strong>REST Services</strong>: The <strong>net</strong> object of the <strong>react-native-force</strong> module makes it easy to invoke the Salesforce Rest services. Check out the <a href="https://github.com/ccoenraets/employee-directory-react-native-salesforce/blob/master/app/services/employee-service-salesforce.js">employee-service-salesforce</a> module for details.</li>
</ul>
