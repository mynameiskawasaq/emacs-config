<p class="alert alert-info"><a href="//raibledesigns.com/rd/entry/getting_started_with_angular_2_rc1">Click here</a> to see an updated version of this tutorial that's been upgraded for Angular 2.0 RC1.</p>
<p>I was hired by one of my current clients in November to help them develop a project management
    application with AngularJS. I'm proud to say we've built the application, it looks great, and it's
    scheduled to be released next month. The team had lots of experience with ExtJS, but was new to AngularJS.
    While using AngularJS worked, they're keen on moving to Angular 2 shortly after it's released.
</p>
<p>
    To help them learn Angular 2, I decided to write a couple tutorials similar to the AngularJS tutorials
    I wrote last year. In this tutorial, I did my best to keep the functionality and features similar
    to <a href="http://raibledesigns.com/rd/entry/getting_started_with_angularjs">Getting Started with AngularJS</a>
    so you can compare <a href="https://github.com/mraible/angular-tutorial">the code</a> between the two.
</p>
<div class="sect1">
    <h3 id="what_you_ll_build">What you'll build</h3>
    <div class="sectionbody">
        <div class="paragraph">
            <p>You'll build a simple web application with Angular 2 and TypeScript. You'll add search and edit features with mock data.</p>
        </div>
    </div>
</div>
<div class="sect1">
    <h3 id="what_you_ll_need">What you'll need</h3>
    <div class="sectionbody">
        <div class="ulist">
            <ul>
                <li>About 15-30 minutes.
                </li>
                <li>A favorite text editor or IDE. I recommend <a href="https://www.jetbrains.com/idea/">IntelliJ IDEA</a>.
                </li>
                <li><a href="http://git-scm.com/">Git</a> installed.
                </li>
                <li><a href="http://nodejs.org/">Node.js</a> and npm installed. I recommend using <a href="https://github.com/creationix/nvm">nvm</a>.
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="sect1">
    <h3 id="create_your_project">Create your project</h3>
    <div class="sectionbody">
        <div class="paragraph">
            <p>Clone the <a href="https://github.com/mgechev/angular2-seed">angular2-seed</a> repository using git:</p>
        </div>
        <div class="listingblock">
            <div class="content">
<pre>git clone https://github.com/mgechev/angular2-seed.git angular2-tutorial
cd angular2-tutorial</pre>
            </div>
        </div>
        <p>
            NOTE: The angular2-seed project requires node v4.x.x or higher and npm 2.14.7. I used node v4.2.6 and npm 3.6.0.
        </p>
        <div class="paragraph">
            <p>Install <code>ts-node</code> for TypeScript:</p>
        </div>
        <div class="listingblock">
            <div class="content">
                <pre>npm install -g ts-node</pre>
            </div>
        </div>
        <div class="paragraph">
            <p>Install the project's dependencies:</p>
        </div>
        <div class="listingblock">
            <div class="content">
                <pre>npm install</pre>
            </div>
        </div>
    </div>
</div>
<div class="sect1">
    <h3 id="run_the_application">Run the application</h3>
    <div class="sectionbody">
        <div class="paragraph">
            <p>The project is configured with a simple web server for development. To start it, run:</p>
        </div>
        <div class="listingblock">
            <div class="content">
                <pre>npm start</pre>
            </div>
        </div>
        <div class="paragraph">
            <p>You should see a screen like the one below at <a href="http://localhost:5555">http://localhost:5555</a>.</p>
        </div>
        <div id="default-homepage" class="imageblock">
            <div style="text-align: center">
                <a href="https://farm2.staticflickr.com/1560/25886693062_4dd41acd3d_c.jpg" title="Default homepage" rel="lightbox[getting-started-with-angular2]" data-href="https://www.flickr.com/photos/mraible/25886693062/in/datetaken-public/"><img src="https://farm2.staticflickr.com/1560/25886693062_4dd41acd3d_z.jpg" width="640" alt="Default homepage"></a>
            </div>
        </div>
        <div class="paragraph">
            <p>You can see your new project's test coverage by running <code>npm test</code>:</p>
        </div>
        <div class="listingblock">
            <div class="content">
<pre>=============================== Coverage summary ===============================
Statements : 86.11% ( 93/108 )
Branches : 48.28% ( 70/145 )
Functions : 100% ( 25/25 )
Lines : 94.32% ( 83/88 )
================================================================================</pre>
            </div>
        </div>
    </div>
</div>
<div class="sect1">
    <h3 id="add_a_search_feature">Add a search feature</h3>
    <div class="sectionbody">
        <div class="paragraph">
            <p>To add a search feature, open the project in an IDE or your favorite text editor. For IntelliJ IDEA, use
                File &gt; New Project &gt; Static Web and point to the directory you cloned angular2-seed to.</p>
        </div>
        <div class="sect2">
            <h3 id="the_basics">The Basics</h3>
            <div class="paragraph">
                <p>Create a file at <code>src/search/components/search.component.html</code> with the following HTML:</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: xml">&lt;h2&gt;Search&lt;/h2&gt;
&lt;form&gt;
  &lt;input type="search" [(ngModel)]="query" (keyup.enter)="search()"&gt;
  &lt;button type="button" (click)="search()"&gt;Search&lt;/button&gt;
&lt;/form&gt;
&lt;div *ngIf="loading"&gt;loading...&lt;/div&gt;
&lt;pre&gt;{{searchResults | json}}&lt;/pre&gt;
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Create <code>src/search/components/search.component.ts</code> to define the <code>SearchComponent</code> and point to this template.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {Component} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/common';
import {ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
  selector: 'sd-search',
  moduleId: module.id,
  templateUrl: './search.component.html',
  directives: [FORM_DIRECTIVES, CORE_DIRECTIVES, ROUTER_DIRECTIVES]
})
export class SearchComponent {
  loading: boolean;
  query: string;
  searchResults: any;

  constructor() {
    console.log('initialized search component');
  }
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Update <code>src/app/components/app.component.ts</code> to import this component and include its route.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {SearchComponent} from '../../search/components/search.component';

@RouteConfig([
  { path: '/',      name: 'Home',  component: HomeComponent  },
  { path: '/about', name: 'About', component: AboutComponent },
  { path: '/search', name: 'Search', component: SearchComponent }
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Your browser should refresh automatically, thanks to <a href="http://browsersync.io">Browsersync</a>. Navigate to
                    <a href="http://localhost:5555/search">http://localhost:5555/search</a> and you should see the search component.</p>
            </div>
            <div id="search-component" class="imageblock">
                <div style="text-align: center">
                    <a href="https://farm2.staticflickr.com/1552/25374766404_b0830ff897_c.jpg" title="Search component" rel="lightbox[getting-started-with-angular2]" data-href="https://www.flickr.com/photos/mraible/25374766404/in/datetaken-public/"><img src="https://farm2.staticflickr.com/1552/25374766404_b0830ff897_z.jpg" width="640" alt="Search component"></a>
                </div>
            </div>
            <div class="paragraph">
                <p>You can see it needs a bit of styling. Angular 2 allows you to provide styles specific for your component using a <code>styleUrls</code>
                    property on your component.
                    Add this property to <code>search.component.ts</code> you see below.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">templateUrl: './search.component.html',
styleUrls: ['./search.component.css'],
directives: [FORM_DIRECTIVES, CORE_DIRECTIVES, ROUTER_DIRECTIVES]
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Create <code>src/search/components/search.component.css</code> and add some CSS.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: css">:host {
  display: block;
  padding: 0 16px;
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>There, that looks better!</p>
            </div>
            <div class="imageblock">
                <div style="text-align: center">
                    <a href="https://farm2.staticflickr.com/1563/25886691122_c209b90e33_c.jpg" title="Search component with styling" rel="lightbox[getting-started-with-angular2]" data-href="https://www.flickr.com/photos/mraible/25886691122/in/datetaken-public/"><img src="https://farm2.staticflickr.com/1563/25886691122_c209b90e33_z.jpg" width="640" alt="Search component with styling"></a>
                </div>
            </div>
            <div class="paragraph">
                <p>Finally, update <code>src/app/components/navbar.component.html</code> to include a link to the search route.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: xml">&lt;nav&gt;
  &lt;a [routerLink]="['Home']"&gt;HOME&lt;/a&gt;
  &lt;a [routerLink]="['About']"&gt;ABOUT&lt;/a&gt;
  &lt;a [routerLink]="['Search']"&gt;SEARCH&lt;/a&gt;
&lt;/nav&gt;
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>This section has shown you how to add a new component to a basic Angular 2 application.
                    The next section shows you how to create a use a JSON file and <code>localStorage</code> to create a fake API.</p>
            </div>
        </div>
        <div class="sect2">
            <h3 id="the_backend">The Backend</h3>
            <div class="paragraph">
                <p>To get search results, create a <code>SearchService</code> that makes HTTP requests to a JSON file. Start
                    by creating <code>src/shared/data/people.json</code> to hold your data.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">[
  {
    "id": 1,
    "name": "Peyton Manning",
    "phone": "(303) 567-8910",
    "address": {
      "street": "1234 Main Street",
      "city": "Greenwood Village",
      "state": "CO",
      "zip": "80111"
    }
  },
  {
    "id": 2,
    "name": "Demaryius Thomas",
    "phone": "(720) 213-9876",
    "address": {
      "street": "5555 Marion Street",
      "city": "Denver",
      "state": "CO",
      "zip": "80202"
    }
  },
  {
    "id": 3,
    "name": "Von Miller",
    "phone": "(917) 323-2333",
    "address": {
      "street": "14 Mountain Way",
      "city": "Vail",
      "state": "CO",
      "zip": "81657"
    }
  }
]
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Create <code>src/shared/services/search.service.ts</code> and provide <code>Http</code> as a dependency in its constructor.
                    In this same file, define the <code>Address</code> and <code>Person</code> classes that JSON will be marshalled to.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';

@Injectable()
export class SearchService {
  constructor(private http:Http) {}

  getAll() {
    return this.http.get('shared/data/people.json').map((res:Response) =&gt; res.json());
  }
}

export class Address {
  street:string;
  city:string;
  state:string;
  zip:string;

  constructor(obj?:any) {
    this.street = obj &amp;&amp; obj.street || null;
    this.city = obj &amp;&amp; obj.city || null;
    this.state = obj &amp;&amp; obj.state || null;
    this.zip = obj &amp;&amp; obj.zip || null;
  }
}

export class Person {
  id:number;
  name:string;
  phone:string;
  address:Address;

  constructor(obj?:any) {
    this.id = obj &amp;&amp; Number(obj.id) || null;
    this.name = obj &amp;&amp; obj.name || null;
    this.phone = obj &amp;&amp; obj.phone || null;
    this.address = obj &amp;&amp; obj.address || null;
  }
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>In <code>search.component.ts</code>, add imports for these classes.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {Person, SearchService} from '../../shared/services/search.service';
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>You can now add a type to the searchResults variable. While you're there, modify the constructor to inject the
                    <code>SearchService</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">searchResults: Array&lt;Person&gt;;

constructor(public searchService: SearchService) {}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Then implement the <code>search()</code> method to call the service's <code>getAll()</code> method.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">search(): void {
  this.searchService.getAll().subscribe(
    data =&gt; {this.searchResults = data;},
    error =&gt; console.log(error)
  );
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>At this point, you'll likely see the following message in your browser's console.</p>
            </div>
            <div class="listingblock">
                <div class="content">
                    <pre>EXCEPTION: No provider for SearchService! (SearchComponent -&gt; SearchService)</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>This happens because the app hasn't provided this service to components. To fix this, modify
                    <code>app.component.ts</code> to import this component and add the service to the list of providers.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {NameListService} from '../../shared/services/name-list.service';
import {SearchService} from '../../shared/services/search.service';

@Component({
  selector: 'sd-app',
  viewProviders: [NameListService, SearchService],
  moduleId: module.id,
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Next, you'll likely get an error about the <code>Http</code> dependency in <code>SearchService</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
                    <pre>EXCEPTION: No provider for Http! (SearchComponent -&gt; SearchService -&gt; Http)</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>To solve this problem, modify <code>src/main.ts</code> to import the <code>Http</code> service and make it
                    available to the app.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {HTTP_PROVIDERS} from 'angular2/http';

bootstrap(AppComponent, [
  HTTP_PROVIDERS, ROUTER_PROVIDERS,
  provide(APP_BASE_HREF, { useValue: '&lt;%= APP_BASE %&gt;' })
]);
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Now the page will load without errors. However, when you click on the button, you'll see the following error.</p>
            </div>
            <div class="listingblock">
                <div class="content">
                    <pre>ORIGINAL EXCEPTION: TypeError: this.http.get(...).map is not a function</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>I was stuck here for quite some time when I first encountered this issue. I was able to solve it
                    with a simple import in <code>main.ts</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
                    <pre class="brush: js">import 'rxjs/add/operator/map';</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Now clicking the search button should work. To make the results look better,
                    remove the <code>&lt;pre&gt;</code> tag in <code>search.component.html</code>
                    and replace it with a <code>&lt;table&gt;</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: xml">&lt;table *ngIf="searchResults"&gt;
  &lt;thead&gt;
  &lt;tr&gt;
    &lt;th&gt;Name&lt;/th&gt;
    &lt;th&gt;Phone&lt;/th&gt;
    &lt;th&gt;Address&lt;/th&gt;
  &lt;/tr&gt;
  &lt;/thead&gt;
  &lt;tbody&gt;
  &lt;tr *ngFor="#person of searchResults; #i=index"&gt;
    &lt;td&gt;{{person.name}}&lt;/td&gt;
    &lt;td&gt;{{person.phone}}&lt;/td&gt;
    &lt;td&gt;{{person.address.street}}&lt;br/&gt;
      {{person.address.city}}, {{person.address.state}} {{person.address.zip}}
    &lt;/td&gt;
  &lt;/tr&gt;
  &lt;/tbody&gt;
&lt;/table&gt;
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Then add some additional CSS for this component in <code>search.component.css</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: css">table {
  margin-top: 10px;
  border-collapse: collapse;
}

th {
  text-align: left;
  border-bottom: 2px solid #ddd;
  padding: 8px;
}

td {
  border-top: 1px solid #ddd;
  padding: 8px;
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Now the search results look better.</p>
            </div>
            <div id="search-results" class="imageblock">
                <div style="text-align: center">
                    <a href="https://farm2.staticflickr.com/1677/25706917270_bf07c541e4_c.jpg" title="Search results" rel="lightbox[getting-started-with-angular2]" data-href="https://www.flickr.com/photos/mraible/25706917270/in/datetaken-public/"><img src="https://farm2.staticflickr.com/1677/25706917270_bf07c541e4_z.jpg" width="640" alt="Search results"></a>
                </div>
            </div>
            <div class="paragraph">
                <p>But wait, we still don't have search functionality! To add a search feature, add a <code>search()</code> method to
                    <code>search.service.ts</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">search(q:string) {
  if (!q || q === '*') {
    q = '';
  } else {
    q = q.toLowerCase();
  }
  return this.getAll().map(data =&gt; {
    let results = [];
    data.map(item =&gt; {
      if (JSON.stringify(item).toLowerCase().includes(q)) {
        results.push(item);
      }
    });
    return results;
  });
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Then refactor <code>SearchComponent</code> to call this method with its <code>query</code> variable.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">search(): void {
  this.searchService.search(this.query).subscribe(
    data =&gt; {this.searchResults = data;},
    error =&gt; console.log(error)
  );
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Now search results will be filtered by the query value you type in.</p>
            </div>
            <div class="paragraph">
                <p>This section showed you how to fetch and display search results. The next section builds on this and shows how to edit and save a record.</p>
            </div>
        </div>
        <div class="sect2">
            <h3 id="add_an_edit_feature">Add an edit feature</h3>
            <div class="paragraph">
                <p>Modify <code>search.component.html</code> to add a link for editing a person.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: xml">&lt;td&gt;&lt;a [routerLink]="['Edit', { id: person.id }]"&gt;{{person.name}}&lt;/a&gt;&lt;/td&gt;
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Create <code>src/search/components/edit.component.html</code> to display an editable form. You might notice I've added <code>id</code>
                    attributes to most elements. This is to
                    make things easier when writing integration tests with Protractor.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: xml">&lt;div *ngIf="person"&gt;
  &lt;h3&gt;{{editName}}&lt;/h3&gt;
  &lt;div&gt;
    &lt;label&gt;Id:&lt;/label&gt;
    {{person.id}}
  &lt;/div&gt;
  &lt;div&gt;
    &lt;label&gt;Name:&lt;/label&gt;
    &lt;input [(ngModel)]="editName" id="name" placeholder="name"/&gt;
  &lt;/div&gt;
  &lt;div&gt;
    &lt;label&gt;Phone:&lt;/label&gt;
    &lt;input [(ngModel)]="editPhone" id="phone" placeholder="Phone"/&gt;
  &lt;/div&gt;
  &lt;fieldset&gt;
    &lt;legend&gt;Address:&lt;/legend&gt;
    &lt;address&gt;
      &lt;input [(ngModel)]="editAddress.street" id="street"&gt;&lt;br/&gt;
      &lt;input [(ngModel)]="editAddress.city" id="city"&gt;,
      &lt;input [(ngModel)]="editAddress.state" id="state" size="2"&gt;
      &lt;input [(ngModel)]="editAddress.zip" id="zip" size="5"&gt;
    &lt;/address&gt;
  &lt;/fieldset&gt;
  &lt;button (click)="save()" id="save"&gt;Save&lt;/button&gt;
  &lt;button (click)="cancel()" id="cancel"&gt;Cancel&lt;/button&gt;
&lt;/div&gt;
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Create an <code>EditComponent</code> in <code>src/search/components/edit.component.ts</code> that references this template and handles communication with the <code>SearchService</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {Component, OnInit} from 'angular2/core';
import {Person, Address, SearchService} from '../../shared/services/search.service';
import {RouteParams, Router} from 'angular2/router';
import {CanDeactivate, ComponentInstruction} from 'angular2/router';

@Component({
  selector: 'sd-edit',
  moduleId: module.id,
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, CanDeactivate {

  person: Person;
  editName: string;
  editPhone: string;
  editAddress: Address;

  constructor(
    private _service: SearchService,
    private _router: Router,
    private _routeParams: RouteParams
  ) { }

  ngOnInit() {
    let id = +this._routeParams.get('id');
    this._service.get(id).subscribe(person =&gt; {
      if (person) {
        this.editName = person.name;
        this.editPhone = person.phone;
        this.editAddress = person.address;
        this.person = person;
      } else {
        this.gotoList();
      }
    });
  }

  routerCanDeactivate(next: ComponentInstruction, prev: ComponentInstruction): any {
    if (!this.person || this.person.name === this.editName || this.person.phone === this.editPhone
      || this.person.address === this.editAddress) {
      return true;
    }

    return new Promise&lt;boolean&gt;((resolve, reject) =&gt; resolve(window.confirm('Discard changes?')));
  }

  cancel() {
    this._router.navigate(['Search']);
  }

  save() {
    this.person.name = this.editName;
    this.person.phone = this.editPhone;
    this.person.address = this.editAddress;
    this._service.save(this.person);
    this.gotoList();
  }

  gotoList() {
    if (this.person) {
      this._router.navigate(['Search', { term: this.person.name }]);
    } else {
      this._router.navigate(['Search']);
    }
  }
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Modify <code>SearchService</code> to contain functions for finding a person by their id, and saving them. While you're in there, modify
                    the <code>search()</code> method to
                    be aware of updated objects in <code>localStorage</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">search(q:string) {
  if (!q || q === '*') {
    q = '';
  } else {
    q = q.toLowerCase();
  }
  return this.getAll().map(data =&gt; {
    let results = [];
    data.map(item =&gt; {
      // check for item in localStorage
      if (localStorage['person' + item.id]) {
        item = JSON.parse(localStorage['person' + item.id]);
      }
      if (JSON.stringify(item).toLowerCase().includes(q)) {
        results.push(item);
      }
    });
    return results;
  });
}

get(id: number) {
  return this.getAll().map(all =&gt; {
    if (localStorage['person' + id]) {
      return JSON.parse(localStorage['person' + id]);
    }
    return all.find(e =&gt; e.id === id);
  });
}

save(person: Person) {
  localStorage['person' + person.id] = JSON.stringify(person);
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>To make the app are of this new component, add an import and route configuration in <code>app.component.ts</code>.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">import {EditComponent} from '../../search/components/edit.component';

@RouteConfig([
  { path: '/',      name: 'Home',  component: HomeComponent  },
  { path: '/about', name: 'About', component: AboutComponent },
  { path: '/search', name: 'Search', component: SearchComponent },
  { path: '/edit/:id', name: 'Edit', component: EditComponent }
])
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Then create <code>src/search/components/edit.component.css</code> to make the form look a bit better.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: css">:host {
  display: block;
  padding: 0 16px;
}

button {
  margin-top: 10px;
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>At this point, you should be able to search for a person and update their information.</p>
            </div>
            <div id="edit-form" class="imageblock">
                <div style="text-align: center">
                    <a href="https://farm2.staticflickr.com/1603/25886692692_42abb78ef6_c.jpg" title="Edit component" rel="lightbox[getting-started-with-angular2]" data-href="https://www.flickr.com/photos/mraible/25886692692/in/datetaken-public/"><img src="https://farm2.staticflickr.com/1603/25886692692_42abb78ef6_z.jpg" width="640" alt="Edit component"></a>
                </div>
            </div>
            <div class="paragraph">
                <p>The &lt;form&gt; in <code>src/search/components/edit.component.html</code> calls a <code>save()</code> function to update a person's
                    data. You already implemented this above.
                    The function calls a <code>gotoList()</code> function that appends the person's name to the URL when sending the user back to the
                    search screen.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">gotoList() {
  if (this.person) {
    this._router.navigate(['Search', { term: this.person.name }]);
  } else {
    this._router.navigate(['Search']);
  }
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>Since the <code>SearchComponent</code> doesn't execute a search automatically when you execute this URL, add the following logic to do
                    so in its constructor.</p>
            </div>
            <div class="listingblock">
                <div class="content">
<pre class="brush: js">constructor(public searchService: SearchService, params: RouteParams) {
  if (params.get('term')) {
    this.query = decodeURIComponent(params.get('term'));
    this.search();
  }
}
</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>You'll need to import <code>RouteParams</code> in order for everything to compile.</p>
            </div>
            <div class="listingblock">
                <div class="content">
                    <pre class="brush: js">import {ROUTER_DIRECTIVES, RouteParams} from 'angular2/router';</pre>
                </div>
            </div>
            <div class="paragraph">
                <p>After making all these changes, you should be able to search/edit/update a person's information. If it works - nice job!</p>
            </div>
        </div>
    </div>
</div>
<div class="sect1">
    <h3 id="source_code">Source code</h3>
    <div class="sectionbody">
        <div class="paragraph">
            <p>A completed project with this code in it is available on GitHub at <a href="https://github.com/mraible/angular2-tutorial">https://github.com/mraible/angular2-tutorial</a>.
                  If you have ideas for improvements, please leave a comment or send a pull request. 
            </p>
            <p>
                I originally wrote this tutorial in <a href="http://asciidoctor.org/">Asciidoctor</a> because it has a slick feature where you
                can include the source code from files rather than copying and pasting. Unfortunately, GitHub
                <a href="https://github.com/github/markup/issues/172">doesn't support includes</a>. You can
                <a href="http://gist.asciidoctor.org/?github-mraible%2Fangular2-tutorial%2F%2FREADME.adoc">use DocGist to view this tutorial</a>,
                but <a href="https://github.com/asciidoctor/docgist/issues/11">includes don't work</a> there either.
            </p>
            <p>If you'd like to see the Asciidoctor-generated version of this tutorial, you can install the gem, checkout the project from GitHub,
                and then run <code>asciidoctor README.adoc</code> to produce a <code>README.html</code> file.
            </p>
        </div>
    </div>
</div>
<div class="sect1">
    <h3 id="summary">Summary</h3>
    <div class="sectionbody">
        <div class="paragraph">
            <p>I hope you've enjoyed this quick-and-easy tutorial on how to get started with Angular 2. In a future tutorial,
                I'll show you <a href="//raibledesigns.com/rd/entry/testing_angular_2_applications">how to write unit tests and integration tests for this application</a>. I've also started looking into creating an ES6 version of this tutorial using Soós Gábor's <a href="https://github.com/blacksonic/angular2-es6-starter">angular2-es6-starter</a>. If you know of a better starter for Angular 2 and ES6, please let me know.</p>
        </div>
        <div class="sect2">
            <h3 id="resources">Resources</h3>
            <div class="paragraph">
                <p>I used a number of resources while creating this application. <a href="https://www.ng-book.com/2/">ng-book 2</a> was an invaluable resource
                    and I highly recommend it if you're learning Angular 2. I found Chariot Solution's article on
                    <a href="http://chariotsolutions.com/blog/post/angular2-observables-http-separating-services-components/">Angular2 Observables, Http, and
                        separating services and components</a>
                    to be quite helpful. Finally, the <a href="https://github.com/angular/angular-cli">angular-cli</a> project was a big help, especially its
                    <code>ng generate route &lt;object&gt;</code> feature.</p>
            </div>
            <div class="paragraph">
                <p>Kudos to all the pioneers in Angular 2 land that've been using it and writing about it on blogs and Stack Overflow.
                    Getting started with Angular 2 would've been a real pain without your trailblazing.</p>
            </div>
        </div>
    </div>
</div>