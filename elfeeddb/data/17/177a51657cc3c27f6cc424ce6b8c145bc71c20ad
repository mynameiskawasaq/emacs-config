<p>
<a href="http://www.infoq.com/minibooks/jhipster-mini-book">
<img src="http://www.infoq.com/resource/minibooks/jhipster-mini-book/en/cover/Cover.jpg" alt="The JHipster Mini-Book" width="150" class="picture" style="border: 1px solid black"></a>
Last Friday, the <a href="http://www.infoq.com/minibooks/jhipster-mini-book">JHipster Mini-Book was published on
    InfoQ</a>. I wrote about this milestone on the <a
    href="http://www.jhipster-book.com/#!/news/entry/jhipster-mini-book-released">book's blog</a>. I'm
    pumped to see this release happen, and I'd like to give you a behind-the-scenes peak at how it went from idea to production.
</p>
<p id="the-idea"><strong>The Idea</strong><br>
    At the end of last year, I wrote down my goals for 2015:
</p>
<ul>
    <li>21 Point Fitness App</li>
    <li>JHipster Mini Book (InfoQ)</li>
    <li>Finish Bus</li>
    <li>New House</li>
    <li>Good Blood Pressure</li>
</ul>
<p>My reason for wanting to write a JHipster Mini-Book was simple: I knew AngularJS, Bootstrap and Spring Boot quite
    well. I'd used them on several projects and I really liked how <a href="http://jhipster.org">JHipster</a> married them all together. I often ran into
    people that used these technologies, but hadn't heard of JHipster. I was hoping to make more people aware of the
    project and market my development skills at the same time.
</p>
<p>The first step to accomplishing a goal is writing it down. After writing it down and sharing with friends, I sent
    an email to <a href="http://www.infoq.com/author/Charles-Humble">Charles Humble</a>, InfoQ's Head of Editorial, in
    early January. Charles directed me to InfoQ's internal mini-book guide and recommended I create an outline and talk to the
    project owners. It took me six weeks before I created an outline and sent it to
    <a href="http://www.julien-dubois.com/">Julien Dubois</a>. We went back-and-forth on the outline for a while and
    finalized it around the beginning of April.
</p>
<p>
    On April 8, I delivered a <a href="//raibledesigns.com/rd/entry/getting_hip_with_jhipster_at">talk on JHipster</a>
    at Denver's Java Users Group and announced I was writing the book.
</p>

<p id="asciidoctor"><strong>The Development Process</strong><br>
    Early on, I'd decided I wanted to write the book with AsciiDoc and build it with <a href="http://asciidoctor.org/">Asciidoctor</a>.
    I did receive some slight push back (to use Google Docs) from InfoQ, but they were mostly supportive. Their biggest
    concern was getting it into a desktop publishing program and formatting it to fit their style. I have an email that
    says "this is going to be the most expensive book we’ve ever produced!".
</p>
<p>
    I chose Asciidoctor because I wanted to run the book development and publishing process like an open source project.
    I also found its ability to import source code quite handy. I used the <a href="https://github.com/asciidoctor/asciidoctor-gradle-examples">
    asciidoctor-gradle-examples project</a> as a starting point, specifically the <a href="https://github.com/asciidoctor/asciidoctor-gradle-examples/tree/master/asciidoc-to-all-example">
    asciidoctor-to-all-example</a>. I hadn't used Gradle on a project before, so I was motivated to learn it. Now that I think of it,
    I never even bothered to research how to use Asciidoctor with Maven. </p>
<p>
    I used JIRA in the cloud ($20/month) to create tasks, organize sprints and track my progress. I broke things down by chapter,
    starting with the <em>UI components</em> section, followed by <em>Building an app</em>, and <em>API building blocks</em>.
    I used the outline of the book to create my tasks. I gave myself a couple weeks for each chapter, but it ended up
    taking about a month each. I wrote the preface, introduction and everything else after the main content had been
    tech edited and was being copy edited.
</p>
<p>
    I created a private repository on <a href="https://bitbucket.org/">Bitbucket</a> to host the book's source control in Git.
    The only reason I chose Bitbucket over GitHub was because Bitbucket offers free private repositories. It's a shame
    they have <a href="https://bitbucket.org/site/master/issues/7132/render-asciidoc-files-bb-1913#comment-22554834">
    no plans to build a custom renderer for AsciiDoc</a>.
</p>
<p>
    I wrote the book using IntelliJ IDEA and used a combination of <code>./gradlew watch</code> and Grunt with
    <a href="http://browsersync.io">Browsersync</a> to refresh my browser when changes were made.
</p>
<p id="tech-editing"><strong>Tech Editing</strong><br>
    I sent my tech editors, <a href="https://twitter.com/sharpedennis">Dennis Sharpe</a> and <a href="https://twitter.com/kileniklawski">Kile Niklawski</a>,
    emails when I finished with a chapter. I gave them access to the book's repo, as well as the sample application's repo, and
    detailed how to access them. I made sure and documented how to build both projects in their README files. I asked
    Dennis and Kile to enter issues, or create branches and pull requests when they found problems. They entered a few issues on the first
    chapter, but responded via email after that. They didn't find many issues, so this process was good enough for me.
</p>

<p id="copy-editing"><strong>Copy Editing</strong><br>
    Lawrence Nyveen copy-edited the book and was gracious enough to try my recommended process:</p>
<ul>
    <li>Checkout the source code of the book from Git.</li>
    <li>Create a branch, make changes.</li>
    <li>Publish the branch, create a pull request.</li>
    <li>Have a conversation about the changes on the pull request, make more changes, merge pull request.</li>
</ul>
<p>
This process seemed to work very well and Laurie quickly learned how to work with Git. We both
used <a href="http://asciidocfx.com/">AsciidocFX</a> to edit and make changes. We used comments in the book's
code to ask questions to each other. AsciidocFX worked quite well, though we did find we needed to add
<code>:imagesdir: images</code> to chapter headers to get them to render images. Having problems with image
rendering was not new, I had to create a symlink in the <em>chapters</em> directory to the images directory
above it to get images to render in EPUB.
</p>
<p>From the time Laurie started copy editing until he was finished was only a couple weeks.
</p>

<p id="publishing"><strong>Publishing</strong><br>
    As I mentioned earlier, making the output look like InfoQ's mini-books was one of the biggest concerns with this book.
    InfoQ wanted to take the HTML (or PDF) version of the book, feed it into their desktop publishing program, and
    publish from there. I expressed my concerns in an email:
</p>
<blockquote class="quote">
    The JHipster project moves pretty fast, so it'll probably be a lot of work to pump out releases as quickly as they do. 
    It's probably better to do a release per quarter with updated material. If you're going to reformat it every time we do a
    release, that could create a lot of work.
</blockquote>
<p>They agreed and mentioned the output from Asciidoctor was looking pretty good. They asked if I could make it match
the standard InfoQ look and feel. I did some research, found out it
    <a href="//raibledesigns.com/rd/entry/customizing_an_asciidoctor_pdf_so">might be possible</a>, and
    <a href="//raibledesigns.com/rd/entry/re_customizing_an_asciidoctor_pdf">went to work</a>. It took me a week of
    research and a week of trial-and-error to produce the <a href="http://www.infoq.com/minibooks/jhipster-mini-book">
    final product</a>.
</p>

<p id="next"><strong>What's Next?</strong><br>
    The good news is the first (1.0) version of the book is in production. I have not published
    <a href="http://www.21-points.com/">21-Points Health</a> (the example application
    for the book) as an open source project, but I may in the future. For now, I hope to do my best to keep the book (and the app) 
    up-to-date with JHipster releases. That may prove difficult, but we'll see.
</p>
<p>
    One of the things I recommend in the book is to follow <a href="https://github.com/johnpapa/angular-styleguide">John Papa's Angular Style Guide</a>.
    A recent <a href="https://github.com/jhipster/generator-jhipster/pull/2256">pull request</a> does this for JHipster. I think this is an
    excellent enhancement, but it'd also require me to rewrite many of the JavaScript sections of the book. It sounds like a daunting task,
    but at least it's a mini-book!
</p>
<p>
    If you have any questions, comments or suggestions about writing with Asciidoctor or publishing with InfoQ, send them my way.
    My fastest responses will likely come from Twitter (<a href="https://twitter.com/jhipster-book">@jhipster_book</a> or
    <a href="https://twitter.com/mraible">@mraible</a>). You can also leave a comment here, on <a href="http://www.jhipster-book.com">jhipster-book.com</a>,
    or post a question on Stack Overflow with the "jhipster-mini-book" tag.
</p>
<p>P.S. If you live in Denver, there's a <a href="http://www.meetup.com/DOSUG1/events/221213046/">presentation on Asciidoctor</a> by
    <a href="https://twitter.com/tlberglund">Tim Berglund</a> and <a href="https://twitter.com/cojorado">Cara Jo Miller</a> tonight. I'll also be talking about
    <a href="http://cfp.devoxx.be/2015/talk/KGX-5093/Get_Hip_with_JHipster:_Spring_Boot_+_AngularJS_+_Bootstrap">JHipster at Devoxx</a> next Wednesday.</p>
