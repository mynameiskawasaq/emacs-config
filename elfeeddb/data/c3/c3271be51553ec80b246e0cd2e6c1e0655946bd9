<p><img src="/images/reinventbanner.jpg"/ width="650"></p>

<p>In my keynote at AWS re:Invent today, I announced 13 new features and services (in addition to the 15 we announced yesterday).</p>

<p>My favorite parts of James Bond movies is are where 007 gets to visit Q to pick up and learn about new tools of the trade: super-powered tools with special features which that he can use to complete his missions, and, in some cases, get out of some nasty scrapes. Bond always seems to have the perfect tool for every situation that he finds himself in. *</p>

<p>At AWS, we want to be the Q for developers, giving them the super-powered tools and services with deep features in the Cloud. In the hands of builders, the impact of these services has been to completely transform the way applications are developed, debugged, and delivered to customers.</p>

<p>I was joined by 32,000 James Bonds at the conference today from all around the world, and we introduced new services focused on accelerating this transformation across development, testing and operations, data and analytics, and computation itself.</p>

<p><strong>Transformation in Development, Testing, &amp; Operations</strong></p>

<p>Although development and operations are often overlooked, they are the engines of agility for most organizations. Today, cCompanies cannot afford to wait two or three years between releases, and; customers have found that continually releasing incremental functionality to customer frequently reduces risk and improves quality.</p>

<p>Today, we&#39;re making available broad new services which that let builders prepare and operate their applications more quickly and efficiently, and respond to changes in both their business and their operating environment, swiftly. We launched the following new services and features today to help.</p>

<p><a href="https://blog.chef.io/2016/12/01/inside-the-new-aws-opsworks-for-chef-automate-service/"><strong>AWS OpsWorks for Chef</strong></a> <strong>:</strong> a fully managed Chef Automate environment, available through AWS OpsWorks to fuel even more automation and reduce the heavy lifting associated with continuous deployment.</p>

<p><a href="https://aws.amazon.com/about-aws/whats-new/2016/12/introducing-ec2-systems-manager-for-automated-configuration-management-of-ec2-and-on-premises-systems/"><strong>Amazon EC2 Systems Manager</strong></a> <strong>:</strong> A collection of tools for package installation, patching, resource configuration, and task automation on Amazon EC2.</p>

<p><a href="https://aws.amazon.com/blogs/aws/aws-codebuild-fully-managed-build-service/"><strong>AWS Codebuild</strong></a>: A new, fully managed, extensible service for compiling source code and running unit tests, which is integrated with other application lifecycle management services— such as AWS CodeDeploy, AWS CodeCommit, and AWS CodePipeline— for dramatically decreasing the time between iterations of software.</p>

<p><a href="https://aws.amazon.com/blogs/aws/aws-x-ray-see-inside-of-your-distributed-application/"><strong>Amazon X-Ray</strong></a>: A new service to analyze, visualize, and debug distributed applications, allowing builders to identify performance bottlenecks and errors.</p>

<p><a href="https://aws.amazon.com/blogs/aws/new-aws-personal-health-dashboard-status-you-can-relate-to/"><strong>Personal Health Dashboard</strong></a>: A new personalized view of AWS service health for all customers, allowing developers to gain visibility into service health issues which that may be affecting their application.</p>

<p><a href="https://aws.amazon.com/blogs/aws/aws-shield-protect-your-applications-from-ddos-attacks/"><strong>AWS Shield</strong></a> <strong>:</strong> protective Protective armor against distributed denial of service (DDoS) attacks, available as Shield Standard and Shield Advanced. Shield Standard gives DDoS protection to all customers using API Gateway, Elastic Load Balancing, Route 53, CloudFront, and EC2. Shield Advanced protects against more sophisticated DDoS attacks, with access to help through a 24x7 AWS DDoS response team.</p>

<p><strong>Transformation in Data</strong></p>

<p>In the old world, access to infrastructure resources was a big differentiator for big, wealthy companies. No more. Today, any developer can have access to a wealth of infrastructure technology services which that bring advanced technology to their fingertips times in the Cloud. The days of differentiation through infrastructure are behind us; the technology is now evenly distributed.</p>

<p>Instead, most companies today and in the future will differentiate themselves through the data that they collect and have access to, and the way in which they can put that data to work for the benefit of their customers. We rolled out three new services today to make that easier.:</p>

<p><a href="https://aws.amazon.com/blogs/aws/amazon-pinpoint-hit-your-targets-with-aws/"><strong>Amazon Pinpoint</strong></a> <strong>:</strong> A data-driven engagement service for mobile apps. Define which segment of customers to engage with, schedule a push notification engagement campaign, and track the results in real-time.</p>

<p><a href="https://aws.amazon.com/blogs/aws/aws-batch-run-batch-computing-jobs-on-aws/"><strong>AWS Batch</strong></a>: Fully- managed batch processing at any scale, with no batch processing software to install or servers to manage.</p>

<p>Dynamically provision compute resources and optimize task distribution based on volume and resource requirements</p>

<p><a href="https://aws.amazon.com/about-aws/whats-new/2016/12/introducing-aws-glue-prepare-and-load-data-into-data-stores/"><strong>AWS Glue</strong></a> <strong>:</strong> A fully- managed data catalog and ETL service, which that makes it easy to move data between data stores, while also simplifying and automating time time-consuming data discovery, conversion, mapping, and job scheduling tasks.</p>

<p><strong>Transformation in Compute</strong></p>

<p>Amazon EC2 made it possible to build application architectures in a way we had always wanted to; and, over the past decade, gave us the opportunity to build secure, resilient, available applications with decoupled application components which that can be scaled independently, and updated more frequently. When I talk to our customers, I hear time and again how they are taking these transformative principles, and taking them to the next level, by building smaller, more discrete, distributed components using containers and AWS Lambda.</p>

<p>Today, we&#39;re accelerating this transformation with a new distributed application coordination service, new Lambda functionality, and an open source container framework.:</p>

<p><a href="https://aws.amazon.com/blogs/aws/new-aws-step-functions-build-distributed-applications-using-visual-workflows/"><strong>AWS Step Functions</strong></a>: Coordinate the components of distributed applications using visual workflows. Step through functions at scale.</p>

<p><a href="https://aws.amazon.com/blogs/aws/coming-soon-lambda-at-the-edge/"><strong>Lambda@Edge</strong></a>: Enable Lambda functions in Edge locations, and run functions in response to CloudFront events. We also <a href="https://aws.amazon.com/about-aws/whats-new/2016/12/aws-lambda-supports-c-sharp/">added C# support</a> for AWS Lambda.</p>

<p><a href="https://aws.amazon.com/blogs/aws/blox-new-open-source-scheduler-for-amazon-ec2-container-service/"><strong>Blox</strong></a>: A collection of open source projects for container management and orchestration.</p>

<p>Thirteen new services and major features focused on developers. We&#39;re excited to see how customers you will put these new features to work.</p>

<p>*: Sean Connery is the definitive Bond.</p>
