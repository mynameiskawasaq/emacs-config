<p>This is the second part of my series of articles describing how to make Emacs a
great JavaScript development environment.  This time we’ll focus on getting good
auto-completion with type inference.</p>

<p>If you haven’t read it yet, you should jump to
the <a href="https://emacs.cafe/emacs/javascript/setup/2017/04/23/emacs-setup-javascript.html">first post</a>
first to get things started.</p>

<h2 id="setting-up-tern--company-mode-for-auto-completion">Setting up Tern &amp; company-mode for auto-completion</h2>

<p><a href="https://ternjs.net">Tern</a> is a great tool once setup correctly.  It parses
JavaScript files in a project and does type inference to provide meaningful
completion (with type hints) and support for cross-references.</p>

<p>Unfortunately, cross-references with tern never reliably worked for me, that’s
why I have always been
using <a href="https://github.com/nicolaspetton/xref-js2">xref-js2</a> instead for that
(see <a href="https://emacs.cafe/emacs/javascript/setup/2017/04/23/emacs-setup-javascript.html">part #1</a>).</p>

<p>For auto-completion, we’ll be using company-mode with tern.  Let’s go ahead and
install tern:</p>

<div class="highlighter-rouge"><pre class="highlight"><code>$ sudo npm install -g tern
</code></pre>
</div>

<p>Now let’s install the Emacs packages:</p>

<div class="highlighter-rouge"><pre class="highlight"><code>M-x package-install RET company-tern RET
</code></pre>
</div>

<p>The Emacs configuration is straight-forward, we simply enable company-mode with
the tern backend for JavaScript buffers:</p>

<figure class="highlight"><pre><code class="language-elisp"><span class="p">(</span><span class="nb">require</span> <span class="ss">'company-mode</span><span class="p">)</span>
<span class="p">(</span><span class="nb">require</span> <span class="ss">'company-tern</span><span class="p">)</span>

<span class="p">(</span><span class="nv">add-to-list</span> <span class="ss">'company-backends</span> <span class="ss">'company-tern</span><span class="p">)</span>
<span class="p">(</span><span class="nv">add-hook</span> <span class="ss">'js2-mode-hook</span> <span class="p">(</span><span class="k">lambda</span> <span class="p">()</span>
                           <span class="p">(</span><span class="nv">tern-mode</span><span class="p">)</span>
                           <span class="p">(</span><span class="nv">company-mode</span><span class="p">)))</span>
                           
<span class="c1">;; Disable completion keybindings, as we use xref-js2 instead</span>
<span class="p">(</span><span class="nv">define-key</span> <span class="nv">tern-mode-keymap</span> <span class="p">(</span><span class="nv">kbd</span> <span class="s">"M-."</span><span class="p">)</span> <span class="no">nil</span><span class="p">)</span>
<span class="p">(</span><span class="nv">define-key</span> <span class="nv">tern-mode-keymap</span> <span class="p">(</span><span class="nv">kbd</span> <span class="s">"M-,"</span><span class="p">)</span> <span class="no">nil</span><span class="p">)</span></code></pre></figure>

<p>Now, depending on your JavaScript project, you might want to setup tern to work
with your project structure. If completion doesn’t work out of the box using
tern defaults you will have to set it up using a <code class="highlighter-rouge">.tern-project</code> placed in the
root folder containing your JavaScript files.</p>

<p>Here’s an example setup for a project that uses <code class="highlighter-rouge">requirejs</code> and <code class="highlighter-rouge">jQuery</code>,
ignoring files from the <code class="highlighter-rouge">bower_components</code> directory:</p>

<figure class="highlight"><pre><code class="language-json"><span class="p">{</span><span class="w">
  </span><span class="nt">"libs"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="s2">"jquery"</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="nt">"loadEagerly"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="s2">"./**/*.js"</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="nt">"dontLoad"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="s2">"./bower_components/"</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="nt">"plugins"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="nt">"requirejs"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
      </span><span class="nt">"baseURL"</span><span class="p">:</span><span class="w"> </span><span class="s2">"./"</span><span class="w">
    </span><span class="p">}</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span></code></pre></figure>

<p>Once setup, tern offers superb completion. Together with company-mode, you get
great context-based completion with type inference.</p>

<p><img alt="Ternjs" src="https://emacs.cafe/img/tern-company-mode.png" /></p>

<p>When completing a function, you can hit <code class="highlighter-rouge">&lt;F1&gt;</code> to get its documentation:</p>

<p><img alt="Ternjs documentation" src="https://emacs.cafe/img/tern-documentation.png" /></p>

<h2 id="until-next-time">Until next time</h2>

<p>In the next articles I’ll cover linting with Flycheck, <code class="highlighter-rouge">gulp</code> and <code class="highlighter-rouge">grunt</code>
integration into Emacs, and of course how to setup and
use <a href="https://indium.readthedocs.io">Indium</a>.</p>