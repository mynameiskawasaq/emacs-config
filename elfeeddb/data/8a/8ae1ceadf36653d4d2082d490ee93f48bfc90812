<div class="paragraph">
<p>We are very pleased to announce the availability of the first milestone of the Spring for Apache Kafka 2.0 release <a href="https://github.com/spring-projects/spring-kafka/milestone/13?closed=1">2.0.0.M1</a>.</p>
</div>
<div class="paragraph">
<p>Significant new features in the 2.0 line include:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>Support for timestamps in the <code>KafkaTemplate</code>.</p>
</li>
<li>
<p>Seek to beginning/end of topics.</p>
</li>
<li>
<p>New threading model facilitated by <a href="https://cwiki.apache.org/confluence/display/KAFKA/KIP-62%3A+Allow+consumer+to+send+heartbeats+from+a+background+thread">KIP-62</a> - now that the consumer client does not rely on <code>poll()</code> being called frequently, the threading model is much simpler; avoiding the need for internal <code>pause()</code> / <code>resume()</code> processing.
Listeners are now always invoked on the consumer thread. This, in turn, has facilitated:</p>
</li>
<li>
<p><code>ConsumerAwareMessageListener</code> (and <code>BatchConsumerAwareMessageListener</code>) are provided so that listener implementations can access the <code>Consumer&lt;?, ?&gt;</code> object to perform operations such as <code>pause()</code>, <code>resume()</code>, <code>metrics()</code> etc.</p>
</li>
<li>
<p><code>@KafkaListener</code> POJO methods can now be annotated with <code>@SendTo</code> to send the method result to some other topic.</p>
</li>
</ul>
</div>
<div class="listingblock">
<div class="content">
<pre class="prettyprint highlight"><code class="language-java" data-lang="java">@KafkaListener(id = "replyingListener", topics = "inTopic")
@SendTo("replyTopic")
public String replyingListener(String in) {
	return in.toUpperCase();
}</code></pre>
</div>
</div>
<div class="paragraph">
<p>For more information, see <a href="http://docs.spring.io/spring-kafka/docs/2.0.0.BUILD-SNAPSHOT/reference/html/_reference.html#annotation-send-to">Forwarding Listener Results using @SendTo</a>.</p>
</div>
<div class="ulist">
<ul>
<li>
<p><code>@KafkaListener</code> annotations now have an <code>errorHandler</code> property, allowing a custom error handler to be configured for each. Previously, you had to use a different container factory for each.</p>
</li>
<li>
<p>The embedded kafka server JUnit <code>@Rule</code> in <code>spring-kafka-test</code> can now be provided as a Spring Bean instead (and auto wired into your tests).
For further simplification, the framework can auto-declare the bean for you; simply add <code>@EmbeddedKafka</code> to your test class.
See <a href="http://docs.spring.io/spring-kafka/docs/2.0.0.BUILD-SNAPSHOT/reference/html/_reference.html#__embeddedkafka_annotation">@EmbeddedKafka Annotation</a> for more information.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>In addition, the first 3.0 milestone of the Spring Integration Kafka extension is available (3.0.0.M1) based on this <code>spring-kafka</code> milestone.</p>
</div>
<div class="paragraph">
<p>The milestone releases for both projects are available in the <a href="http://repo.spring.io/milestone/">Spring milestone repository</a>.</p>
</div>
<div class="paragraph">
<p>We very much appreciate the feedback (and contributions) received to-date.</p>
</div>
<div class="admonitionblock note">
<table>
<tr>
<td class="icon">
<div class="title">Note</div>
</td>
<td class="content">
Unfortunately, the spring-kafka pom has an incorrect transitive dependency on <code>spring-messaging-5.0.0.BUILD-SNAPSHOT</code>. We don&#8217;t recommend using milestones in production, but in order to provide a stable platform for testing, you should override this dependency to <code>5.0.0.M5</code>.
</td>
</tr>
</table>
</div>
<div class="paragraph">
<p>General availability of the 2.0 release is expected to be in the early summer (shortly after the Spring Framework 5.0 release).
Feedback, feature requests and, of course, contributions are welcomed via the usual channels:</p>
</div>
<div class="paragraph">
<p><a href="http://projects.spring.io/spring-kafka/">Project Page</a> | <a href="https://github.com/spring-projects/spring-kafka/issues">Issues</a> | <a href="https://github.com/spring-projects/spring-kafka/blob/master/CONTRIBUTING.adoc">Contributing</a> | <a href="http://stackoverflow.com/questions/tagged/spring-kafka">Help</a> | <a href="https://gitter.im/spring-projects/spring-kafka">Chat</a></p>
</div>