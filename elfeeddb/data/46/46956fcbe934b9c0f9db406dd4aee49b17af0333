<blockquote><em>As someone who's been accused of "not being a team player" because I had the temerity to say, "No, I can't come in on short notice on a day I've called off, because I'm busy,", <strong>Snoofle</strong>'s rant struck a nerve. I lend him the soapbox for today. -- Remy</em></blockquote><hr>

<p>When you're very young, your parents teach you to say <i>please</i> and <i>thank you</i>. It's good-manners 101. Barking <i>give me ...</i>, <i>get me ...</i> or <i>I want...</i> usually gets you some sort of reprimand. Persistent rudeness yields reprimands of increasing sternness such as no dessert, no TV, etc. Ideally, once learned, those manners should follow us into the grown-up world.
</p>

<img itemprop="image" src="/images/remy/missmanners.jpg" alt="The cover of Miss Manners' Guide for the Turn-Of-The-Millenium" title="It was this or Emily Post, and I didn't think anyone would care about the salad fork." class="inset">

<p>
Should.
</p>

<p>
When you work in IT, particularly in the financial industry, especially on critical systems on which all firm-trading is based, you tend to work with people who think that they are the only people on Earth, and that their individual problem is more important than everyone and everything else. You make a whole lot of money, but you get a lot of abuse from both users and managers. Over time, you develop a pretty thick skin and learn to not take anything personally. Of course, eventually you get tired of dealing with the same old arrogance. Late last summer, I had finally had it with the Wall Street stupidity and decided to retire.
</p>

<p>
After a few months of taking it easy, I started to do volunteer work teaching elderly people the basics of technology. It's not exactly challenging to explain the fundamental differences between Notepad, Word, e-mail, IM, Skype and so forth, but every once in a while, one of the 1940's-set <i>gets</i> it, and it just makes it all worthwhile.
</p>

<p>
One day, I ran into someone who runs a large retail operation. He offered me a part time job to do routine work in his warehouse-type store, and I could work whatever hours I wanted. It's rote, mindless work, but I get to meet and talk to new people all the time (which I think is great) so I accepted. 
</p>

<p>
After a couple of weeks of doing this and the volunteer work, I noticed a vast difference in how people treat you compared with work in IT. For one thing, when people ask you to do something, they start with the word <i>please</i> and finish by saying <i>thank you</i>. Another is that when something needs to be done, they don't assume "Magic Happens Here"; they actually try and figure out how much effort is involved in the task before promising someone else that it will be done after some arbitrary time interval.
</p>

<p>
Nearly four decades in IT has allowed me to accumulate a rather large box of assorted PC parts and cables. When someone has a problem with their machine, more often than not, I can find something in that box that fixes their problem. The women bake me pies, and I've gotten more than a few bottles of booze in appreciation. At the box store, when customers ask you a question, they don't want to tell you what they think; they actually want to hear the answer. How often does that happen in IT?
</p>

<p>
When something happens when I'm not around and people call me for help, they're actually apologetic for interrupting my personal time and ask if it's convenient or if there's a better time to talk. When was the last time anyone at work interrupted you in the middle of the night or weekend, and assumed that you might be doing something, I don't know, <i>personal</i>?
</p>

<p>
I realize the IT industry is comparatively new, but it's been around for more than <i>fifty</i> years at this point. You'd think that after a half century that people might have learned that technical people are not there to be abused, and deserve to be treated decently, like anybody else. Instead, they seem to have forgotten those early lessons in manners.
</p>
<div>
	<img src="http://img.thedailywtf.com/images/inedo/otter-icon.png" style="display:block; float: left; margin: 0 10px 10px 0;" /> [Advertisement] 
	<a href="http://inedo.com/otter?utm_source=tdwtf&utm_medium=footerad&utm_term=Q12016&utm_content=Otter2&utm_campaign=otter">Infrastructure as Code</a> built from the start with first-class Windows functionality and an intuitive, visual user interface. <a href="http://inedo.com/otter?utm_source=tdwtf&utm_medium=footerad&utm_term=Q12016&utm_content=Otter2&utm_campaign=otter">Download</a> Otter today!

</div>
<div style="clear: left;"> </div><div class="feedflare">
<a href="http://syndication.thedailywtf.com/~ff/TheDailyWtf?a=e_ueEAVwAfc:t_k8dgBXAhI:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/TheDailyWtf?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/TheDailyWtf/~4/e_ueEAVwAfc" height="1" width="1" alt=""/>