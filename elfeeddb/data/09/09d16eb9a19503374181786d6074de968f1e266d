<p>
    I've been excited to show people <a href="http://jhipster.github.io/">JHipster</a> and what it can do ever since I
    <a href="//raibledesigns.com/rd/entry/getting_started_with_jhipster_on">started
        using it in September 2014</a>. I've been using its core frameworks (AngularJS,
    Bootstrap and Spring Boot) for a few years and believe they do a great job to
    simplify web development. Especially for Java developers.
</p>
<p>
    When my JHipster talk was accepted for <a href="http://www.devoxx.be/">Devoxx Belgium</a>, I told Trish we were
    headed back to Belgium. She smiled from ear-to-ear. Belgium is one of our favorite countries
    to visit. In an effort to live healthier prior to Devoxx, I stopped drinking beer a month beforehand. I mentioned
    this to friends the week prior.
</p>

<blockquote class="quote">
    <p style="margin-top: 0">One month ago, I stopped drinking beer. I hoped it'd help me with <a href="http://www.21-points.com">www.21-points.com</a>
        and weight loss. Unfortunately, it did not.</p>

    <p style="margin-bottom: 0">
        I told myself I'd start drinking beer again when 1) The Bus was finished or 2) Trish and I arrived in Belgium
        for Devoxx. Looks like #2 will win (we land on Tuesday).</p>
</blockquote>

<p>We arrived in Brussels late Tuesday morning and hopped aboard a train to Antwerp. After
    arriving, we were hungry so we stopped at <a href="http://www.biercentral.eu/">Bier Central</a> for lunch. The
    mussels and
    beer were splendid.</p>

<p style="text-align: center">
    <a href="https://farm6.staticflickr.com/5814/23079344391_a2c964d0df_c.jpg" title="First beer in over a month, so good!" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/23079344391/"><img src="https://farm6.staticflickr.com/5814/23079344391_a2c964d0df.jpg" width="500" alt="First beer in over a month, so good!" style="border: 1px solid black;"></a>
</p>

<p>
    <a href="https://farm6.staticflickr.com/5809/22446966053_48f252f787_c.jpg" title="Breakfast at Bernardin" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/22446966053/in/dateposted-public/"><img src="https://farm6.staticflickr.com/5809/22446966053_48f252f787_t.jpg" width="100" class="picture" alt="Breakfast at Bernardin" style="border: 1px solid black;"></a>
    We walked to our accommodations afterward, the <a href="http://www.bernardin-antwerpen.be/">Gernardin Guesthouse</a>.
    We loved the small space, steep stairs and the nice use of space for the restroom in the upstairs closet. The
    breakfast was delightful too.
</p>

<p>Wednesday afternoon we found ourselves strolling on a city walk around Antwerp. It was overcast, but not chilly
    and we had a fabulous lunch at <a href="http://www.monantwerp.com/">Món</a> after taking some pictures from the top
    of the <a href="http://www.mas.be/" title="Museum aan de Stroom">MAS</a>.</p>
<p style="text-align: center">
    <a href="https://farm6.staticflickr.com/5720/23054582742_393f044782_c.jpg" title="Groenplaats, Anterp, Belgium" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/23054582742"><img src="https://farm6.staticflickr.com/5720/23054582742_393f044782_q.jpg" width="150" alt="Groenplaats, Anterp, Belgium" style="border: 1px solid black;"></a>
    <a href="https://farm1.staticflickr.com/681/22446999753_d096a6c46e_c.jpg" title="From the top of Mas Museum Aan De Stroom" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/22446999753"><img src="https://farm1.staticflickr.com/681/22446999753_d096a6c46e_q.jpg" width="150" alt="From the top of Mas Museum Aan De Stroom" style="border: 1px solid black; margin-left: 15px;"></a>
    <a href="https://farm1.staticflickr.com/742/22447021683_d2e0c2a5dc_c.jpg" title="Lunch at Món" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/22447021683"><img src="https://farm1.staticflickr.com/742/22447021683_d2e0c2a5dc_q.jpg" width="150" alt="Lunch at Món" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>Wednesday evening, we journeyed to the Devoxx venue to deliver my presentation. The performance went well and I heard
    lots of positive feedback almost immediately. This is one of the things I love about Devoxx: the audience tweets
    like mad and
    feedback is immediate. I also like that the presentation displays are like developers monitors; <em>huge!</em>
</p>
<p style="text-align: center">
    <a href="https://farm1.staticflickr.com/580/23056246016_087f330f4f_c.jpg" title="Scotch" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/23056246016"><img src="https://farm1.staticflickr.com/580/23056246016_087f330f4f_m.jpg" width="240" alt="Scotch" style="border: 1px solid black;"></a>

    <a href="https://farm1.staticflickr.com/589/23082331485_995b0a29de_c.jpg" title="An Old Fashioned Java Developer" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/23082331485"><img src="https://farm1.staticflickr.com/589/23082331485_995b0a29de_m.jpg" width="240" alt="DSC_7788.jpg" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://farm1.staticflickr.com/679/22663956718_a82441bd93_c.jpg" title="JHipster!" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/22663956718"><img src="https://farm1.staticflickr.com/679/22663956718_a82441bd93_m.jpg" width="240" alt="JHipster!" style="border: 1px solid black;"></a>
    <a href="https://farm1.staticflickr.com/744/23093573791_07d2e3a9ac_c.jpg" title="Immediate Feedback" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/23093573791"><img src="https://farm1.staticflickr.com/744/23093573791_07d2e3a9ac_m.jpg" width="240" alt="Immediate Feedback" style="border: 1px solid black; margin-left: 15px;"></a>
</p>

<p>Devoxx made an excellent move this year: they uploaded recordings of talks to the <a href="https://www.youtube.com/channel/UCCBVCTuk6uJrN3iFV_3vurg">Devoxx 2015 channel on YouTube</a>. Amazingly, they
    did it
    within hours for each talk! Because of this modern miracle, you can see <a href="https://www.youtube.com/watch?v=baVOGuFIe9M">Get Hip with
        JHipster on YouTube</a> or watch it below.
</p>
<div style="text-align: center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/baVOGuFIe9M" frameborder="0" style="border: 1px solid black" allowfullscreen=""></iframe>
</div>
<p style="text-align: center">
    <a href="http://www.slideshare.net/mraible/get-hip-with-jhipster-spring-boot-angularjs-bootstrap-devoxx-2015">SlideShare</a>
    |
    <a href="http://static.raibledesigns.com/repository/presentations/Get_Hip_with_JHipster_Devoxx2015.pdf">Download
        PDF</a>
</p>

<p>
    Near the end of my presentation, I announced <a href="https://twitter.com/mraible/status/664498478920388608">
    the source code for 21-Points Health is available on GitHub</a>. I've had quite a few people ask for it as part of
    the <a href="http://www.infoq.com/minibooks/jhipster-mini-book">JHipster Mini-Book</a> and it seemed like the right
    thing to do. We celebrated that night with <a href="https://twitter.com/starbuxman">Josh Long</a> and other new friends at Bier Central.</p>
<p style="text-align: center">
    <a data-flickr-embed="true" href="https://farm1.staticflickr.com/595/22445469834_21210403fb_c.jpg" title="Java Hipsters!" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/22445469834"><img src="https://farm1.staticflickr.com/595/22445469834_21210403fb_t.jpg" width="100" alt="Java Hipsters!" style="border: 1px solid black;"></a>

    <a data-flickr-embed="true" href="https://farm1.staticflickr.com/579/23042227126_7e05081502_c.jpg" title="More Cowbell!" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/23042227126"><img src="https://farm1.staticflickr.com/579/23042227126_7e05081502_t.jpg" width="100" alt="More Cowbell!" style="border: 1px solid black; margin-left: 15px;"></a>

    <a data-flickr-embed="true" href="https://farm6.staticflickr.com/5748/22445489504_d700abc8a8_c.jpg" title="New Friends at Bier Central" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/22445489504"><img src="https://farm6.staticflickr.com/5748/22445489504_d700abc8a8_t.jpg" width="100" alt="New Friends at Bier Central" style="border: 1px solid black; margin-left: 15px;"></a>

    <a data-flickr-embed="true" href="https://farm1.staticflickr.com/757/22675994129_aa9b7cfb79_c.jpg" title="Cheers!" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mraible/22675994129"><img src="https://farm1.staticflickr.com/757/22675994129_aa9b7cfb79_t.jpg" width="100" alt="Cheers!" style="border: 1px solid black; margin-left: 15px;"></a>

</p>
<p>
    Thursday we visited Bruges and had a wonderful time strolling around <a href="https://bezoekers.Bruges.be/en/minnewaterpark">Minnewater</a>,
    marveling at the buildings near the main square and taking a clip-clop tour through
    town. We barely made it to the <a href="http://www.brugesbeermuseum.com/">Bruges Beer Museum</a> before it
    closed and had a delicious
    meal at <a href="http://www.cambrinus.eu/">Cambrinus</a>.
</p>
<p style="text-align: center">
    <a href="https://farm1.staticflickr.com/730/22461168593_40d5063088_c.jpg" title="Minnewater Brugge Belgium" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/22461168593"><img src="https://farm1.staticflickr.com/730/22461168593_40d5063088.jpg" width="500" alt="Minnewater Brugge Belgium" style="border: 1px solid black;"></a>
</p>
<p style="text-align: center">
    <a href="https://farm6.staticflickr.com/5725/22459600664_cf3df2638f_c.jpg" title="Sint-Janshospitaal Brugge West-Vlaanderen" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/22459600664"><img src="https://farm6.staticflickr.com/5725/22459600664_cf3df2638f_m.jpg" width="240" alt="Sint-Janshospitaal Brugge West-Vlaanderen" style="border: 1px solid black;"></a>

    <a href="https://farm1.staticflickr.com/688/22690129029_e3da2e47a0_c.jpg" title="View from Mariastraat Brugge Belgium" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/22690129029"><img src="https://farm1.staticflickr.com/688/22690129029_e3da2e47a0_m.jpg" width="240" alt="View from Mariastraat Brugge Belgium" style="border: 1px solid black; margin-left: 15px;"></a>

</p>
<p style="text-align: center">
    <a href="https://farm6.staticflickr.com/5752/22690142739_24b443fc6a_c.jpg" title="Stadhuis Brugge Belgium" rel="lightbox[devoxx2015]" data-href="https://www.flickr.com/photos/mcginityphoto/22690142739"><img src="https://farm6.staticflickr.com/5752/22690142739_24b443fc6a.jpg" width="500" alt="Stadhuis Brugge Belgium" style="border: 1px solid black;"></a>
</p>
<div style="margin: 0 auto; text-align: right; margin-top: -10px; max-width: 500px; font-size: .9em">
    More on Flickr &rarr; My <a href="https://www.flickr.com/photos/mraible/albums/72157660511719478">Devoxx 2015
    Album</a> and Trish's <a href="https://www.flickr.com/photos/mcginityphoto/albums/72157661317992265">Belgium
    November 2015 Album</a>
</div>
<p>
    Thanks to the Devoxx crew for a fun conference and great venue. Thanks to Belgium:
    for being so beautiful, for making savory chocolate, brewing delicious beer and
    for your wonderful people. And to the Java community: thanks
    for being so enthusiastic and fun to talk to. We love creating lasting memories
    with you! &#127867;&#128522;
</p>