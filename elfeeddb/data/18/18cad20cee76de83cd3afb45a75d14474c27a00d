<p>
  I like speaking at conferences. I don't enjoy the stress of creating a new talk and delivering it for the first time, but I do enjoy delivering talks, and I love the feeling after. It's even better when the conference provides an atmosphere that creates lasting memories.
</p>
<p>
  I've been to many conferences in my career. A conference with a sense of community provides one of my favorite experiences. Not just for the people that attend, but for the people that speak. I've been to several conferences that provide this experience and I'm happy to say I just attended one of my favorites: <a href="http://www.jfokus.se/jfokus/">Jfokus 2017</a>.
</p>

<p>
  I flew from Denver to Stockholm last Monday and performed my first talk on Testing Angular Applications just a few hours after I arrived on Tuesday. Usually, I take a day or two to recover from jet lag, but this time I figured I could <em>clutch up</em> and make it work. Going to sleep on the plane at 6pm Denver time certainly helped and I think the talk went well. For the live coding part of the presentation, I used the second half of my <a href="http://gist.asciidoctor.org/?github-mraible/ng-demo//README.adoc">Angular and Angular CLI tutorial</a>. I posted my slides for this talk to <a href="https://www.slideshare.net/mraible/testing-angular-applications-jfokus-2017">SlideShare</a> and <a href="https://speakerdeck.com/mraible/testing-angular-applications-jfokus-2017">Speaker Deck</a>. You can also view them below.
</p>

<script async="" class="speakerdeck-embed" data-id="a7a26351937e43c28726ee54e41b8c3a" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

<p>
  Tuesday night, there was a conference party. I met many new people and put some names to faces with a vibrant community of conference attendees and speakers.
</p>
<div style="max-width: 500px; margin: 0 auto">
  <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Late night <a href="https://twitter.com/hashtag/Jfokus?src=hash">#Jfokus</a> party! <a href="https://t.co/IqjczVXuP6">pic.twitter.com/IqjczVXuP6</a></p>— Matt Raible (@mraible) <a href="https://twitter.com/mraible/status/829107799883935748">February 7, 2017</a></blockquote>
  <script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>
<p>
  I enjoyed Juergen Hoeller's <a href="http://www.jfokus.se/jfokus/talks.jsp#SpringFramework5%3AThe">talk about Spring 5</a> on Wednesday morning. There's a lot of innovation still happening within Spring and it was nice to get a sense of when releases will be happening.
</p>

<div style="max-width: 500px; margin: 0 auto">
  <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Spring Framework 5.0 RC1 in April 2017, GA in May. <a href="https://twitter.com/springboot">@springboot</a> 2.0 M1 in April too. Spring 5.1 with Servlet 4, Spring Boot 2.0 GA in Q4.</p>— Matt Raible (@mraible) <a href="https://twitter.com/mraible/status/829251588824952832">February 8, 2017</a></blockquote>
</div>

I attended a talk on <a href="http://slides.com/gerardsans/jfokus-amazing-ng2-router#/">The Amazing Angular Router</a> with <a href="https://twitter.com/gerardsans">Gerard Sans</a> and then took a stroll around Stockholm in the crisp, cool sunshine.

<p style="text-align: center">

  <a href="https://c1.staticflickr.com/1/590/32901142996_b0f48b2b45_c.jpg" title="Nice day for a stroll around Stockholm." rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32901142996/in/datetaken-public/"><img src="https://c1.staticflickr.com/1/590/32901142996_b0f48b2b45_q.jpg" width="150" alt="Nice day for a stroll around Stockholm." style="border: 1px solid black;"></a>

  <a href="https://c1.staticflickr.com/1/749/32901134576_9e7f16c9ea_c.jpg" title="Nice day for a stroll around Stockholm." rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32901134576/in/datetaken-public/"><img src="https://c1.staticflickr.com/1/749/32901134576_9e7f16c9ea_q.jpg" width="150" alt="Nice day for a stroll around Stockholm." style="border: 1px solid black; margin-left: 15px;"></a>

  <a href="https://c1.staticflickr.com/3/2662/32098301434_4a8318a3c5_c.jpg" title="Nice day for a stroll around Stockholm." rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32098301434/in/datetaken-public/"><img src="https://c1.staticflickr.com/3/2662/32098301434_4a8318a3c5_q.jpg" width="150" alt="Nice day for a stroll around Stockholm." style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
  I gave my talk on PWAs with <a href="https://blog.ionic.io/announcing-ionic-2-0-0-final/">Ionic 2</a>, Angular, and Spring Boot on Wednesday afternoon. It was my first time doing the talk. I really enjoyed this talk because it inspired me to create <a href="https://github.com/stormpath/stormpath-sdk-angular-ionic">Ionic pages for Stormpath</a> to simplify the demo. Also, developing a mobile app with Ionic 2 has been on my technology bucket list for the last year. You can find my slides for this presentation on <a href="https://www.slideshare.net/mraible/testing-angular-applications-jfokus-2017">SlideShare</a> and <a href="https://speakerdeck.com/mraible/testing-angular-applications-jfokus-2017">Speaker Deck</a>. The app I wrote during the presentation is <a href="https://github.com/stormpath/stormpath-spring-boot-ionic-example">available on GitHub</a>, as is <a href="https://github.com/stormpath/stormpath-spring-boot-ionic-example/blob/master/TUTORIAL.md">the tutorial I used to create it</a>.</p>

<script async class="speakerdeck-embed" data-id="bfe7647b20fe42889c869955c7899e8c" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

<p>
  Little did I know, the community aspect of Jfokus was just getting started. When I was invited to the post-Jfokus Speaker's Conference, I leapt at the opportunity. I didn't do it because I thought I'd learn a bunch of stuff, I did it because it offered skiing - one of my favorite activities.
</p><p>
  It turned out to be an incredible experience. I knew some of the other speakers, but I certainly didn't know all of them. Sharing a room with a stranger caused him to become a friend. The views while skiing were magical. The coziness of the lodge where we stayed and the beauty of its atrium were mesmerizing.
</p>
<p>

</p><p style="text-align: center">
  <a href="https://c1.staticflickr.com/3/2212/32098286464_d3d55d7192_c.jpg" title="Sunrise at Storhogna" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32098286464/in/datetaken-public/"><img src="https://c1.staticflickr.com/3/2212/32098286464_d3d55d7192_m.jpg" width="240" alt="Sunrise at Storhogna" style="border: 1px solid black;"></a>
  <a href="https://c1.staticflickr.com/4/3869/32788113292_c7d359d8f2_c.jpg" title="It was a beautiful day for skiing at Klövsjö!" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32788113292/in/datetaken-public/"><img src="https://c1.staticflickr.com/4/3869/32788113292_c7d359d8f2_m.jpg" width="240" alt="It was a beautiful day for skiing at Klövsjö!" style="border: 1px solid black; margin-left: 15px;"></a>
</p>

<p style="text-align: center">
  <a href="https://c1.staticflickr.com/4/3941/32098282014_079a52f4ce_c.jpg" title="The Atrium at Storhogna" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32098282014/in/datetaken-public/"><img src="https://c1.staticflickr.com/4/3941/32098282014_079a52f4ce_m.jpg" width="240" alt="The Atrium at Storhogna" style="border: 1px solid black;"></a>
  <a href="https://c1.staticflickr.com/4/3866/32098275514_be8417729f_c.jpg" title="The pool and hot tub" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32098275514/in/datetaken-public/"><img src="https://c1.staticflickr.com/4/3866/32098275514_be8417729f_m.jpg" width="240" alt="The pool and hot tub" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
  <a href="https://c1.staticflickr.com/1/754/32127083933_12ec190635_c.jpg" title="Another beautiful sunrise on Saturday" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32127083933/in/datetaken-public/"><img src="https://c1.staticflickr.com/1/754/32127083933_12ec190635.jpg" width="500" alt="Another beautiful sunrise on Saturday" style="border: 1px solid black;"></a>
</p>
<p>
  I skied all three days, but I also had a blast sharing so many conversations with such a fun community of technology professionals. When I wasn't trying to catch up on email, Slack, and customer support tickets, we shared stories about how to best work with distributed development teams. Before enjoying the hot tub and roof-top sauna, we hacked games on Raspberry Pi's and make the zombie apocalypse possible with sensors and buttons.
</p>
<p>On the bus ride back to Stockholm, I sat with <a href="https://twitter.com/saturnism">Ray Tsang</a> and figured out how to do another tech bucket list item: deploying a <a href="https://jhipster.github.io/microservices-architecture/">JHipster microservices architecture</a> to Google Cloud. It wasn't easy with the bumpy road, but we took breaks when we started to feel sick. The bus had wi-fi, making it possible to upload fat Docker images to <a href="https://hub.docker.com/">Docker Hub</a>.
</p>
<div style="max-width: 500px; margin: 0 auto">
  <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Hacking with <a href="https://twitter.com/saturnism">@saturnism</a> on the <a href="https://twitter.com/hashtag/Jfokus?src=hash">#Jfokus</a> bus ride. Got a <a href="https://twitter.com/kubernetesio">@kubernetesio</a> cluster with <a href="https://twitter.com/java_hipster">@java_hipster</a> microservices running on Google Cloud! ???? <a href="https://t.co/wqest06HOu">pic.twitter.com/wqest06HOu</a></p>— Matt Raible (@mraible) <a href="https://twitter.com/mraible/status/830465662183153666">February 11, 2017</a></blockquote>
</div>
<p>
  Before I left Sweden on Sunday, I walked around <a href="https://en.wikipedia.org/wiki/Gamla_stan">The Old Town</a> in Stockholm and took a few pictures.
</p>
<p style="text-align: center">
  <a href="https://c1.staticflickr.com/3/2045/32901161866_6cba35e036_c.jpg" title="A walk through Old Town Stockholm" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32901161866/in/datetaken-public/"><img src="https://c1.staticflickr.com/3/2045/32901161866_6cba35e036_q.jpg" width="150" alt="A walk through Old Town Stockholm" style="border: 1px solid black;"></a>
  <a href="https://c1.staticflickr.com/3/2361/32788127212_ef3767f860_c.jpg" title="A walk through Old Town Stockholm" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32788127212/in/datetaken-public/"><img src="https://c1.staticflickr.com/3/2361/32788127212_ef3767f860_q.jpg" width="150" alt="A walk through Old Town Stockholm" style="border: 1px solid black; margin-left: 15px;"></a>

  <a href="https://c1.staticflickr.com/1/310/32127089833_8216274747_c.jpg" title="A walk through Old Town Stockholm" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32127089833/in/datetaken-public/"><img src="https://c1.staticflickr.com/1/310/32127089833_8216274747_q.jpg" width="150" alt="A walk through Old Town Stockholm" style="border: 1px solid black; margin-left: 15px;"></a>

</p>
<p style="text-align: center">

  <a href="https://c1.staticflickr.com/3/2054/32901166626_6de4876152_c.jpg" title="A beautiful winter morning in Stockholm" rel="lightbox[jfokus2017]" data-href="https://www.flickr.com/photos/mraible/32901166626/in/datetaken-public/"><img src="https://c1.staticflickr.com/3/2054/32901166626_6de4876152.jpg" width="500" alt="A beautiful winter morning in Stockholm" style="border: 1px solid black;"></a>
</p><div style="margin: 0 auto; text-align: right; margin-top: -10px; max-width: 500px; font-size: .9em">
  More on Flickr &#8594; <a href="https://www.flickr.com/photos/mraible/albums/72157676824798794">Jfokus 2017</a>
</div>
<p>
Jfokus was a jolly good time! The conference, the community, a cozy mountain chalet, a fun time skiing and great stimulation for my learning-hungry brain. Thanks to all the organizers, especially <a href="https://twitter.com/matkar">Mattias</a> and <a href="https://twitter.com/javaHelena">Helena</a>. Mattias is an excellent snowboarder and we had lots of fun racing down the mountain. Helena has a beautiful voice and it was super fun to hear her sing into the evening with the accompaniment of <a href="https://twitter.com/javaHelena/status/830193880809209856">Ola's guitar</a>. 
</p>
<div style="max-width: 500px; margin: 0 auto">
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Singing with <a href="https://twitter.com/gotoOla">@gotoOla</a> while <a href="https://twitter.com/saturnism">@saturnism</a> and <a href="https://twitter.com/apnylle">@apnylle</a> is coding at <a href="https://twitter.com/hashtag/jfokus?src=hash">#jfokus</a> speaker conf. The best mix! <a href="https://twitter.com/mraible">@mraible</a>, <a href="https://twitter.com/matkar">@matkar</a> &amp; <a href="https://twitter.com/pehrs">@pehrs</a> sing along! <a href="https://t.co/Xy2ijTnYrF">pic.twitter.com/Xy2ijTnYrF</a></p>&mdash; Helena Hjertén (@javaHelena) <a href="https://twitter.com/javaHelena/status/830193880809209856">February 10, 2017</a></blockquote>
</div>
<p>Lasting memories, I have many.</p>
<p id="update"><strong>Update:</strong> The good folks from <a href="https://www.jfokus.se/jfokus/">Jfokus</a> have published videos of my talks.</p>
<div style="margin: 0 auto; width: 560px">
<iframe width="560" height="315" src="https://www.youtube.com/embed/C_V3Je7Iwso" frameborder="0" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/TksyjxipM4M" frameborder="0" allowfullscreen></iframe>
</div>
