<p>
<a href="https://minecraft.net"><img src="//raibledesigns.com/repository/images/minecraft-logo.png" alt="Minecraft" width="300" class="picture" style="margin-top: -10px"></a>
My 10-year-old son, Jack, is a huge fan of <a href="https://minecraft.net/">Minecraft</a>. If you let him,
    he'd play all day, skipping meals and having a blast. It's most fun to hear him playing with his sister or
    his best friend. I'm amazed it's captured his attention for so long; well over two years. Both my kids loved it when
    Scott Davis taught a <a href="http://www.meetup.com/Devoxx4Kids-Denver">Devoxx4Kids Denver</a> class on
    <a href="//raibledesigns.com/rd/entry/first_devoxx4kids_in_denver_a">Server-side Minecraft programming</a>.
</p>
<p>We haven't had any <a href="http://www.meetup.com/Devoxx4Kids-Denver/">Devoxx4Kids Denver</a> workshops this year,
    but that's about to change. First of all, I'm happy to announce we're working with the
    <a href="http://rmoug.com">Rocky Mountain Oracle Users Group</a> to have a
    <a href="http://www.meetup.com/Devoxx4Kids-Denver/messages/boards/thread/49148693">Day of Family Coding Fun at
        Elitch Gardens</a> this Friday. There will be a workshop on Raspberry Pi and I'll be doing a demonstration
    on how to setup a Minecraft Server in the cloud. Next weekend, we'll be doing a more in-depth Minecraft Workshop at Devoxx4Kids Denver. If you'd like to join us <a href="http://www.meetup.com/Devoxx4Kids-Denver/events/224431746/">please RSVP</a>.

Since having your own Minecraft Server is a fun thing for kids,
    and useful for parents, I figured I'd document how to do it here.</p>

<p>
    First of all, let me say that I'm standing on the shoulders of giants. When I first setup a Minecraft server, I used
    Ben Garton's <a href="http://www.blog.gartonhill.com/setting-up-a-free-minecraft-server-in-the-cloud-part-1/">
    Setting up a free Minecraft server in the cloud - part 1</a> as well as
    <a href="http://www.blog.gartonhill.com/setting-up-a-free-minecraft-server-in-the-cloud-part-2/">
        part 2</a> and <a href="http://www.blog.gartonhill.com/upgrading-your-minecraft-ec2-cloud-server-and-save-money/">3</a>.
    I also found Aaron Bell's <a href="http://www.aaronbell.com/how-to-run-a-minecraft-server-on-amazon-ec2/">
    How to run a Minecraft server on Amazon EC2</a> to be quite useful.
</p>
<p>Without further ado, here's you how to setup a Minecraft Server on Amazon Web Services (AWS) in 2015!</p>
<div id="step1">
    <p><strong>Step 1: Signup for AWS and Create an Instance</strong></p>
    <ol>
        <li>Navigate to <a href="http://aws.amazon.com/">http://aws.amazon.com/</a>,
            and click "Sign In to the Console" using your Amazon account. If you don't have an AWS account, you'll
            need to create one and specify a payment method.
        </li>
        <li><p>
            Click on <strong>EC2</strong> in the top left corner, then <strong>Launch Instance</strong> on the following
            screen.</p>

            <p style="text-align: center">
                <a href="https://farm4.staticflickr.com/3682/20289881856_1b99053e79_c.jpg" title="AWS Console" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20289881856/in/album-72157654487678494/"><img src="https://farm4.staticflickr.com/3682/20289881856_1b99053e79.jpg" width="500" height="377" alt="AWS Console"></a>

            </p>
        </li>
        <li>
            <p>Select <strong>Amazon Linux</strong>.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/332/20307710822_569d21cc0e_c.jpg" title="Select Amazon Linux" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20307710822/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/332/20307710822_569d21cc0e.jpg" width="500" height="377" alt="Select Amazon Linux"></a>

            </p>
        </li>
        <li>
            <p>Choose an Instance Type of <strong>t2.micro</strong>, then click <strong>Next: Configure Instance
                Details</strong>.
            </p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/468/20316123115_e69046be65_c.jpg" title="Instance Type" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20316123115/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/468/20316123115_e69046be65.jpg" width="500" height="364" alt="Instance Type"></a>

            </p>
        </li>
        <li>You don't need to configure anything on the next screen, so click <strong>Next: Add Storage</strong>.
            Storage
            settings don't need to be changed either, so click <strong>Next: Tag Instance</strong>.
        </li>
        <li><p>On the Tag Instance screen, assign a name to your server. I chose "Minecraft Server". Click
            <strong>Next: Configure Security Group</strong> to continue.</p>

            <p style="text-align: center">
                <a href="https://farm4.staticflickr.com/3776/20307710592_de1e0d32a6_c.jpg" title="Tag Instance" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20307710592/in/album-72157654487678494/"><img src="https://farm4.staticflickr.com/3776/20307710592_de1e0d32a6.jpg" width="500" height="280" alt="Tag Instance"></a>

            </p>
        </li>
        <li><p>This step is important because it opens a Minecraft port that allows players to connect. Create a new
            security
            group with name <strong>Minecraft</strong> and description <strong>Ports for Minecraft</strong>. Click
            <strong>
                Add Rule</strong>, specify <strong>Custom TCP Rule</strong>, Port Range <strong>25565</strong> and
            Source
            <strong>Anywhere</strong>. Note that you can also lock down your instance so only certain IPs can connect.
            Click
            <strong>Review and Launch</strong> to continue.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/256/20289881646_ec758dec01_c.jpg" title="Security Group Configuration" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20289881646/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/256/20289881646_ec758dec01.jpg" width="500" height="365" alt="Security Group Configuration"></a>

            </p>
        </li>
        <li><p>You'll be warned about allowing any IP address on the following screen. Click <strong>Launch</strong> to
            continue.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/405/20289882136_a894265e8d_c.jpg" title="Review Instance" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20289882136/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/405/20289882136_a894265e8d.jpg" width="500" height="365" alt="Review Instance"></a>

            </p>

        </li>
        <li>
            <p>You'll be prompted to create a new keypair. I chose "minecraft" for my key pair name. Click <strong>Download</strong> to download your key pair.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/338/20128101798_aec2deac2d_c.jpg" title="Create Key Pair" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20128101798/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/338/20128101798_aec2deac2d.jpg" width="500" height="365" alt="Create Key Pair"></a>

            </p>
            <p>I executed the following commands to move this key to a location on my hard drive and locked it down so
                the
                public can't view it.</p>
<pre class="brush: bash; gutter: false">mv ~/Downloads/minecraft.pem ~/.ssh/.
chmod 400 .ssh/minecraft.pem
</pre>
            <p>Click <strong>Launch Instances</strong> to continue. You should see something like the following screen.
            </p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/439/20128088610_1512c30ddf_c.jpg" title="Launch Status" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20128088610/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/439/20128088610_1512c30ddf.jpg" width="500" height="385" alt="Launch Status"></a>

            </p>
        </li>
        <li>
            <p>Click on the instance name and copy/paste the Public IP. You'll want to write down this IP address since
                you'll need it later, and you'll also want to send it to friends so they can join.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/385/20322138311_7f65ca260c_c.jpg" title="Public IP Address" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20322138311/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/385/20322138311_7f65ca260c.jpg" width="500" height="385" alt="Public IP Address"></a>

            </p>
            <p>Execute the following command with this IP to connect
                to your server. Type <strong>yes</strong> when prompted to continue connecting.</p>
<pre class="brush: bash; gutter: false">ssh -i .ssh/minecraft.pem ec2-user@your-public-ip
</pre>
            <p>You'll likely be told there's a number of updates to install; run <strong>sudo yum update</strong> to
                install them.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/257/20316121795_fd2a052dc9_c.jpg" title="SSH" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20316121795/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/257/20316121795_fd2a052dc9.jpg" width="500" height="210" alt="SSH"></a>

            </p>
        </li>
    </ol>
</div>

<div id="step2">
    <p><strong>Step 2: Install a Minecraft Server</strong></p>
    <ol>
        <li><p>From your Linux prompt, type the following commands to create a folder and copy the latest version* of
            the
            Minecraft server into it.</p>
<pre class="brush: bash; gutter: false">mkdir MinecraftServer
cd MinecraftServer
wget https://s3.amazonaws.com/Minecraft.Download/versions/1.8.8/minecraft_server.1.8.8.jar
</pre>
            <p>* Check <a href="http://www.minecraft.net/download">http://www.minecraft.net/download</a> to find out the
                latest
                version number and change the above command appropriately.</p>
        </li>
        <li>Create a symlink to the downloaded JAR so you can keep the same launch command, regardless of version.
<pre class="brush: bash; gutter: false">ln -s minecraft_server.1.8.8.jar minecraft_server.jar
</pre>
        </li>
        <li><p>Launch your server using the following command:</p>
<pre class="brush: bash; gutter: false">sudo java -Xmx1G -Xms1G -jar minecraft_server.jar nogui
</pre>
            <p>You should see ouput like the screenshot below, prompting you to agree to the EULA.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/510/20322137791_1ba0d416a0_c.jpg" title="EULA Warning" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20322137791/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/510/20322137791_1ba0d416a0.jpg" width="500" height="167" alt="EULA Warning"></a>

            </p>

        </li>
        <li><p>Edit <code>eula.txt</code> by running <strong>sudo vi eula.txt</strong> and changing "eula=false" to
            "eula=true".
            If you're unfamiliar with vi, the following instructions will help you edit this file after you've opened
            it.</p>
            <ul>
                <li>Type "/false" followed by [Return]</li>
                <li>Type "xxxxx" to delete "false"</li>
                <li>[Shift+A] to go to the end of the line</li>
                <li>Type "true"</li>
                <li>Hit [Esc], then type ":wq" to save the file</li>
            </ul>
        </li>
        <li><p>Run the <strong>sudo java</strong> command again (hitting up arrow twice will retrieve this command from
            your history). This time,
            the server should start, albeit with a few warnings about missing files.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/482/20307710482_c0edde8f8b_b.jpg" title="Server launches successfully!" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20307710482/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/482/20307710482_c0edde8f8b_z.jpg" width="511" height="640" alt="Server launches successfully!"></a>

            </p>
        </li>
    </ol>
</div>

<div id="step3"><strong>Step 3: Connect to Your Server and Play!</strong>

    <p>This is the easiest step of all, and possibly one that your kids are familiar with.</p>
    <ol>
        <li><p>Launch Minecraft. Make sure the profile uses the same version as your server. Copy the IP address
            of your server to your clipboard and click <strong>Play</strong>.</p>

            <p style="text-align: center">
                <a href="https://farm1.staticflickr.com/474/20128098228_d1b60e6fc5_c.jpg" title="Minecraft Launcher" rel="lightbox[minecraftserveraws]" data-href="https://www.flickr.com/photos/mraible/20128098228/in/album-72157654487678494/"><img src="https://farm1.staticflickr.com/474/20128098228_d1b60e6fc5.jpg" width="500" height="342" alt="Minecraft Launcher"></a>

            </p>
        </li>
        <li><p>Click <strong>Multiplayer</strong>, followed by <strong>Add Server</strong>. Give it a name you'll
            remember
            and paste the IP address into the <strong>Server Address</strong>. Click <strong>Done</strong>, followed by
            <strong>Join Server</strong>.</p>

            <p>Note: if you want to toggle fullscreen mode, you can do this with F11. If you don't have F11 on your
                keyboard,
                go to Options &gt; Video Settings and click <strong>Fullscreen</strong> to toggle it.
            </p>
        </li>

    </ol>
    <p>Congratulations! You just setup a Minecraft server in the cloud. Now you can send the IP address to friends and
        invite them to play!
    </p>

    <p>
        One of the issues that this setup has is that your server will shut down as soon as you logout of your SSH
        session.
        You can run the Minecraft server and leave it running using the following command.</p>
<pre class="brush: bash; gutter: false">nohup sudo java -Xmx1G -Xms1G -jar minecraft_server.jar nogui &gt; minecraft.log 2&gt;&amp;1 &amp;
</pre>
    <p>
        This will keep everything running in the background, even after you logout. It also spits out a process id you
        can use to stop the server.
    </p><pre class="brush: bash; gutter: false">kill -9 processId</pre>
    <p>If you lose this number, you can find the process id by running <code>ps aux | grep java</code>. You can also
        shutdown all Java processes with
        <code>sudo killall java</code>.
    </p>
</div>
<p>If you have any tips or tricks for improving this tutorial, I'd love to hear about them in the comments.</p>

<p id="next-steps"><strong>Next Steps</strong><br>
    When I first setup a Minecraft server on AWS earlier this year, I never bothered to shut it down. The result was it
    cost me around $15 the first month. From then on, I simply started it whenever my son asked me to, then shut it down when he went to bed.
</p>
<p>
    <a href="http://www.blog.gartonhill.com/upgrading-your-minecraft-ec2-cloud-server-and-save-money/">Ben Garton has a good tutorial</a> on
    how to setup a cron job to shutdown the instance at midnight. He also shows how to start the server using a Desktop shortcut on Windows. If
    you've done something similar for Mac/Linux, I'd love to hear about it. Allowing your kid to fire up their own Minecraft server on demand (and
    shutting it down automatically) seems to be the most economical way to run things.
</p>

<p id="devoxx4kids"><strong>Devoxx4Kids Denver Workshop Next Week</strong><br>
    If you'd like to learn more about Minecraft, developing mods and setting up your own server, you should join us at
    the <a href="http://www.meetup.com/Devoxx4Kids-Denver/events/224431746/">Devoxx4Kids Denver Meetup next week</a> (Saturday, August 15th at 9:30am). We'll be tuning in live to
    Arun and Aditya Gupta's vJUG
    session
    on <a href="http://www.meetup.com/virtualJUG/events/224283882/">Getting Started with Minecraft Modding</a>. In the
    second hour,
    I'll show how to setup your own server on AWS and configure it to have the mods we've developed while watching the
    vJUG session. Thanks to our venue sponsor <a href="http://www.tuliva.com/">Tuliva</a>, you don't even need to bring
    a machine! They have computers available for the kids to use and a sweet location too. <a href="http://www.meetup.com/Devoxx4Kids-Denver/events/224431746/">RSVP today</a>!
</p>

<p>Related: It seems you can also run a Minecraft server on Heroku using <a href="https://github.com/jacobwgillespie/heroku-minecraft">heroku-minecraft</a>.</p>