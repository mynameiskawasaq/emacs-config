<p>This is the first of <strong>n</strong> articles about building <strong>systems</strong> in <strong><a href="http://clojure.org/">Clojure</a></strong>. Clojure is a beautiful language and I have been fascinated from the first moment I laid eyes on it last summer. However, what remained a mystery to me for most of the time was how to build more complex systems. I started researching the options that would allow me to structure an arbitrarily complex application in a way that is <strong>easy to understand and maintain</strong>. Here is what I found.</p>

<!-- more -->


<p>As an example for such a system, we will be looking at the Clojure rewrite of an application I wrote last year: <strong><a href="https://github.com/matthiasn/BirdWatch">BirdWatch</a></strong>. This application subscribes to the <strong><a href="https://dev.twitter.com/docs/streaming-apis">Twitter Streaming API</a></strong> for all tweets that contain one or more terms out of a set of terms and makes the tweets searchable through storing them in ElasticSearch. A live version of the Clojure version of this application is available here: <strong><a href="http://birdwatch2.matthiasnehlsen.com/#*">http://birdwatch2.matthiasnehlsen.com</a></strong>.</p>

<p>In this first installment we will be looking at the basic architecture of the server side. Let&#8217;s start with an animation <sup id="fnref:1"><a href="#fn:1" rel="footnote">1</a></sup> to demonstrate how components in the system get wired up when the application initializes before we go into details.</p>

<br/>




<script language="javascript" type="text/javascript">
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    obj.style.width = obj.contentWindow.document.body.scrollWidth + 'px';
  }
</script>




<iframe width="100%;" src="http://matthiasnehlsen.com/iframes/bw-anim/index.html" scrolling="no" onload="javascript:resizeIframe(this);" ></iframe>




<br/>


<br/>


<p>The architecture above is a huge improvement over the first version and was only possible thanks to Stuart Sierra&#8217;s <strong><a href="https://github.com/stuartsierra/component">component library</a></strong>. This new version has cleanly separated components with no dependencies between namespace at all (except, of course, in the main namespace that wires everything together). But the individual components don&#8217;t know anything about each other except for where the components in the animation touch each other. And even there, it is mostly just plain <strong><a href="https://github.com/clojure/core.async">core.async</a></strong> channels.</p>

<p>In the initial version that I wrote, where everything depended on everything, things were very different. Some people would call that &#8220;spaghetti code&#8221;, but I think that is not doing justice to spaghetti. Unlike bad code, I don&#8217;t mind touching spaghetti. I would rather liken bad code to hairballs, of the worst kind that is. Have you ever experienced the following: you are standing in the shower and the water doesn&#8217;t drain. You notice something in the sink, so you squat down to pull it out only to start screaming, &#8220;Oh my god, it&#8217;s a dead rat&#8221; a second later. I am referring to that kind of entangled hairball mess, nothing less. On top, you may even hit your head when you jump up in disgust.</p>

<p>This is where dependency injection comes in. Can we agree that we don&#8217;t like hairballs? Good. Usually, what we are trying to achieve is a so-called inversion of control, in which a component of the application knows that it will be injected something which implements a known interface at runtime. Then, no matter what the actual implementation is, it knows what methods it can call on that something because of the implemented interface.</p>

<p>Here, unlike in object-oriented dependency injection, things are a little different because we don&#8217;t really have objects. The components play the role of objects, but as a further way of decoupling, I wanted them to only communicate via <strong>core.async</strong> channels. Channels are a great abstraction. Rich Hickey likens them to conveyor belts onto which you put something without having to know at all what happens on the other side. We will have a more detailed look at the channels in the next article. For now, as an abstraction, we can think about the channel components (the flat ones connecting the components with the switchboard) as <strong>wiring harnesses</strong>, like the one that connects the electronics of your car to your engine. The only way to interface with a modern engine (that doesn&#8217;t have separate mechanical controls) is by connecting to this wiring harness and either send or receive information, depending on the channel / cable that you interface with.</p>

<p>Let&#8217;s have a look at how the initialization of the application we have already seen in the animation looks in code:</p>

<figure class='code'><figcaption><span>Main namespace</span><a href='https://github.com/matthiasn/BirdWatch/blob/a26c201d2cc2c89f4b3d2ecb8e6adb403e6f89c7/Clojure-Websockets/src/clj/birdwatch/main.clj'>main.clj</a></figcaption> <div class="highlight"><table><tr><td class="gutter"><pre class="line-numbers"><span class='line-number'>1</span>
<span class='line-number'>2</span>
<span class='line-number'>3</span>
<span class='line-number'>4</span>
<span class='line-number'>5</span>
<span class='line-number'>6</span>
<span class='line-number'>7</span>
<span class='line-number'>8</span>
<span class='line-number'>9</span>
<span class='line-number'>10</span>
<span class='line-number'>11</span>
<span class='line-number'>12</span>
<span class='line-number'>13</span>
<span class='line-number'>14</span>
<span class='line-number'>15</span>
<span class='line-number'>16</span>
<span class='line-number'>17</span>
<span class='line-number'>18</span>
<span class='line-number'>19</span>
<span class='line-number'>20</span>
<span class='line-number'>21</span>
<span class='line-number'>22</span>
<span class='line-number'>23</span>
<span class='line-number'>24</span>
<span class='line-number'>25</span>
<span class='line-number'>26</span>
<span class='line-number'>27</span>
<span class='line-number'>28</span>
<span class='line-number'>29</span>
<span class='line-number'>30</span>
<span class='line-number'>31</span>
<span class='line-number'>32</span>
<span class='line-number'>33</span>
<span class='line-number'>34</span>
<span class='line-number'>35</span>
<span class='line-number'>36</span>
<span class='line-number'>37</span>
<span class='line-number'>38</span>
<span class='line-number'>39</span>
<span class='line-number'>40</span>
</pre></td><td class='code'><pre><code class='clojure'><span class='line'><span class="p">(</span><span class="kd">ns </span><span class="nv">birdwatch.main</span>
</span><span class='line'>  <span class="p">(</span><span class="ss">:gen-class</span><span class="p">)</span>
</span><span class='line'>  <span class="p">(</span><span class="ss">:require</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">birdwatch.twitter-client</span> <span class="ss">:as</span> <span class="nv">tc</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">birdwatch.communicator</span> <span class="ss">:as</span> <span class="nv">comm</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">birdwatch.persistence</span> <span class="ss">:as</span> <span class="nv">p</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">birdwatch.percolator</span> <span class="ss">:as</span> <span class="nv">perc</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">birdwatch.http</span> <span class="ss">:as</span> <span class="nv">http</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">birdwatch.switchboard</span> <span class="ss">:as</span> <span class="nv">sw</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">clojure.edn</span> <span class="ss">:as</span> <span class="nv">edn</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">clojure.tools.logging</span> <span class="ss">:as</span> <span class="nv">log</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">clj-pid.core</span> <span class="ss">:as</span> <span class="nv">pid</span><span class="p">]</span>
</span><span class='line'>   <span class="p">[</span><span class="nv">com.stuartsierra.component</span> <span class="ss">:as</span> <span class="nv">component</span><span class="p">]))</span>
</span><span class='line'>
</span><span class='line'><span class="p">(</span><span class="k">def </span><span class="nv">conf</span> <span class="p">(</span><span class="nf">edn/read-string</span> <span class="p">(</span><span class="nb">slurp </span><span class="s">&quot;twitterconf.edn&quot;</span><span class="p">)))</span>
</span><span class='line'>
</span><span class='line'><span class="p">(</span><span class="kd">defn </span><span class="nv">get-system</span> <span class="p">[</span><span class="nv">conf</span><span class="p">]</span>
</span><span class='line'>  <span class="s">&quot;Create system by wiring individual components so that component/start</span>
</span><span class='line'><span class="s">  will bring up the individual components in the correct order.&quot;</span>
</span><span class='line'>  <span class="p">(</span><span class="nf">component/system-map</span>
</span><span class='line'>   <span class="ss">:communicator-channels</span> <span class="p">(</span><span class="nf">comm/new-communicator-channels</span><span class="p">)</span>
</span><span class='line'>   <span class="ss">:communicator</span>  <span class="p">(</span><span class="nf">component/using</span> <span class="p">(</span><span class="nf">comm/new-communicator</span><span class="p">)</span> <span class="p">{</span><span class="ss">:channels</span> <span class="ss">:communicator-channels</span><span class="p">})</span>
</span><span class='line'>   <span class="ss">:twitterclient-channels</span> <span class="p">(</span><span class="nf">tc/new-twitterclient-channels</span><span class="p">)</span>
</span><span class='line'>   <span class="ss">:twitterclient</span> <span class="p">(</span><span class="nf">component/using</span> <span class="p">(</span><span class="nf">tc/new-twitterclient</span> <span class="nv">conf</span><span class="p">)</span> <span class="p">{</span><span class="ss">:channels</span> <span class="ss">:twitterclient-channels</span><span class="p">})</span>
</span><span class='line'>   <span class="ss">:persistence-channels</span> <span class="p">(</span><span class="nf">p/new-persistence-channels</span><span class="p">)</span>
</span><span class='line'>   <span class="ss">:persistence</span>   <span class="p">(</span><span class="nf">component/using</span> <span class="p">(</span><span class="nf">p/new-persistence</span> <span class="nv">conf</span><span class="p">)</span> <span class="p">{</span><span class="ss">:channels</span> <span class="ss">:persistence-channels</span><span class="p">})</span>
</span><span class='line'>   <span class="ss">:percolation-channels</span> <span class="p">(</span><span class="nf">perc/new-percolation-channels</span><span class="p">)</span>
</span><span class='line'>   <span class="ss">:percolator</span>    <span class="p">(</span><span class="nf">component/using</span> <span class="p">(</span><span class="nf">perc/new-percolator</span> <span class="nv">conf</span><span class="p">)</span> <span class="p">{</span><span class="ss">:channels</span> <span class="ss">:percolation-channels</span><span class="p">})</span>
</span><span class='line'>   <span class="ss">:http</span>          <span class="p">(</span><span class="nf">component/using</span> <span class="p">(</span><span class="nf">http/new-http-server</span> <span class="nv">conf</span><span class="p">)</span> <span class="p">{</span><span class="ss">:communicator</span> <span class="ss">:communicator</span><span class="p">})</span>
</span><span class='line'>   <span class="ss">:switchboard</span>   <span class="p">(</span><span class="nf">component/using</span> <span class="p">(</span><span class="nf">sw/new-switchboard</span><span class="p">)</span> <span class="p">{</span><span class="ss">:comm-chans</span> <span class="ss">:communicator-channels</span>
</span><span class='line'>                                                         <span class="ss">:tc-chans</span> <span class="ss">:twitterclient-channels</span>
</span><span class='line'>                                                         <span class="ss">:pers-chans</span> <span class="ss">:persistence-channels</span>
</span><span class='line'>                                                         <span class="ss">:perc-chans</span> <span class="ss">:percolation-channels</span><span class="p">})))</span>
</span><span class='line'><span class="p">(</span><span class="k">def </span><span class="nv">system</span> <span class="p">(</span><span class="nf">get-system</span> <span class="nv">conf</span><span class="p">))</span>
</span><span class='line'>
</span><span class='line'><span class="p">(</span><span class="kd">defn </span><span class="nv">-main</span> <span class="p">[</span><span class="o">&amp;</span> <span class="nv">args</span><span class="p">]</span>
</span><span class='line'>  <span class="p">(</span><span class="nf">pid/save</span> <span class="p">(</span><span class="ss">:pidfile-name</span> <span class="nv">conf</span><span class="p">))</span>
</span><span class='line'>  <span class="p">(</span><span class="nf">pid/delete-on-shutdown!</span> <span class="p">(</span><span class="ss">:pidfile-name</span> <span class="nv">conf</span><span class="p">))</span>
</span><span class='line'>  <span class="p">(</span><span class="nf">log/info</span> <span class="s">&quot;Application started, PID&quot;</span> <span class="p">(</span><span class="nf">pid/current</span><span class="p">))</span>
</span><span class='line'>  <span class="p">(</span><span class="nf">alter-var-root</span> <span class="o">#</span><span class="ss">&#39;system</span> <span class="nv">component/start</span><span class="p">))</span>
</span></code></pre></td></tr></table></div></figure>


<p>I personally think this <strong>reads really well</strong>, even if you have never seen Clojure before in your life. Roughly the first half is concerned with imports and reading the configuration file. Next, we have the <code>get-system</code> function which declares, what components depend on what other components. The system is finally started in the <code>-main</code> function (plus the process ID logged and saved to a file). This is all there is to know about the application entry point.</p>

<p>Now, when we start the application, all the dependencies will be started in an order that the component library determines so that all dependencies are met. Here&#8217;s the output of that startup process:</p>

<pre><code>mn:Clojure-Websockets mn$ lein run
16:46:30.925 [main] INFO  birdwatch.main - Application started, PID 6682
16:46:30.937 [main] INFO  birdwatch.twitter-client - Starting Twitterclient Channels Component
16:46:30.939 [main] INFO  birdwatch.twitter-client - Starting Twitterclient Component
16:46:30.940 [main] INFO  birdwatch.twitter-client - Starting Twitter client.
16:46:31.323 [main] INFO  birdwatch.persistence - Starting Persistence Channels Component
16:46:31.324 [main] INFO  birdwatch.persistence - Starting Persistence Component
16:46:31.415 [main] INFO  org.elasticsearch.plugins - [Chameleon] loaded [], sites []
16:46:32.339 [main] INFO  birdwatch.communicator - Starting Communicator Channels Component
16:46:32.340 [main] INFO  birdwatch.communicator - Starting Communicator Component
16:46:32.355 [main] INFO  birdwatch.http - Starting HTTP Component
16:46:32.375 [main] INFO  birdwatch.http - Http-kit server is running at http://localhost:8888/
16:46:32.376 [main] INFO  birdwatch.percolator - Starting Percolation Channels Component
16:46:32.377 [main] INFO  birdwatch.percolator - Starting Percolator Component
16:46:32.380 [main] INFO  birdwatch.switchboard - Starting Switchboard Component
</code></pre>

<p>Next week, we will look at how these components wire a channel grid and how information flows through this grid. You do not have to wait to see more code though, everything is on <strong><a href="https://github.com/matthiasn/BirdWatch">GitHub</a></strong> already.</p>

<p>Once we have discussed the architecture in detail over the next couple of weeks, we can start observing the system under load. Of course, it would be interesting to have actual user load. But with or without actual load, we want to find a way of how to generate / simulate load and then observe the system, identify the bottlenecks and remove them. For example, the clients could be simulated by connecting a load generator via ZeroMQ or the like and deliver matches back to that application and check if they are as expected (correct, complete, timely). The Twitter stream could also be simulated, for example by connecting to a load generator that either replays recorded tweets, with full control over the rate, or with artificial test cases, for which we could exactly specify the expectations on the output side.</p>

<p>That&#8217;s it for now. Would you like to be informed when the next article is out? Just <strong>sign up</strong> for the <a href="http://eepurl.com/y0HWv" target="_blank"><strong>mailing list</strong></a> and I will let you know. Also, if you are interested in Clojure, you may want to check out my curated list of <strong><a href="https://github.com/matthiasn/Clojure-Resources">useful Clojure-related resources on GitHub</a></strong>.</p>

<p>Cheers,
Matthias</p>

<p><strong>P.S.</strong> This series of articles is now continued in this book:</p>

<iframe width="160" height="400" src="https://leanpub.com/building-a-system-in-clojure/embed" frameborder="0" allowtransparency="true"></iframe>



<div class="footnotes">
<hr/>
<ol>
<li id="fn:1">
<p>In case you are interested in the animation as such, you can check out the current <strong><a href="http://matthiasnehlsen.com/blog/2014/09/23/weekly-update/">weekly review</a></strong> in which I provide some background information.<a href="#fnref:1" rev="footnote">&#8617;</a></p></li>
</ol>
</div>

